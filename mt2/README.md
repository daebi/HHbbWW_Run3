# MT2 Calculation

MT2 Description https://arxiv.org/pdf/hep-ph/9906349.pdf

A variable trying to constrain MT when 2 neutrinos are present

For our case, we are interested in 3 cases

1. Visible lep1, Visible lep2, Invisible MET+bjet1+bjet2, mass hypothesis1 bjet1 mass hypothesis2 bjet2 mass

2. Visible bjet1, Visible bjet2, Invisible MET+lep1+lep2, mass hypothesis1 Wmass, mass hypothesis2 Wmass

3. Visible bjet1+lep1, Visible bjet2+lep2, Invisible MET, mass hypothesis1 0 mass hypothesis2 0

The script will take the input trees and add the 3 MT2 variables. To call the mt2 calculation,
```
python3 calculate_mt2.py -i input_file.root -o output_file.root
```

