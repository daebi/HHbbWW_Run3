import uproot
import mt2Calculator
import numpy as np
import argparse
import tqdm
import ROOT
import vector

#When calling by hand, the include needs a good pathway
mt2_pathway = '/'.join(__file__.split('/')[:-1])+'/'
ROOT.gInterpreter.Declare('#include "{}Lester_mt2_bisect.cpp"'.format(mt2_pathway))

#Call example
#python3 calculate_mt2.py -i inputFile.root -o outputFile.root

parser = argparse.ArgumentParser(description='mt2 calculator')
parser.add_argument("-i", "--inputFile", dest="infile", type=str, default=[], help="input file name. [Default: [] ]")
parser.add_argument("-o", "--outputFile", dest="outfile", type=str, default="out.root", help="output file name. [Default: 'out.root']")
args, unknown = parser.parse_known_args()

filename = args.infile
outname = args.outfile

f = uproot.open(filename)
outfile = uproot.recreate(outname)

save_single = True

single_keys = []
double_keys = []
for key in f.keys():
    if 'Single' in key: single_keys.append(key.split(';')[0])
    if 'Double' in key: double_keys.append(key.split(';')[0])


mt2 = mt2Calculator.mt2Calculator()
mt2_lester = ROOT.asymm_mt2_lester_bisect()

for key in double_keys:
    t = f[key]
    events = t.arrays()

    #mt2_ll_list = []
    #mt2_bb_list = []
    #mt2_blbl_list = []

    mt2_ll_lester_list = []
    mt2_bb_lester_list = []
    mt2_blbl_lester_list = []

    for ev in tqdm.tqdm(events):
        #mt2.setLeptons(ev.lep0_pt, ev.lep0_eta, ev.lep0_phi, ev.lep1_pt, ev.lep1_eta, ev.lep1_phi)
        #if ev.Double_HbbFat:
        #    mt2.setBJets(ev.ak8_jet0_subjet1_pt, ev.ak8_jet0_subjet1_eta, ev.ak8_jet0_subjet1_phi, ev.ak8_jet0_subjet2_pt, ev.ak8_jet0_subjet2_eta, ev.ak8_jet0_subjet2_phi)
        #else:
        #    mt2.setBJets(ev.ak4_jet0_pt, ev.ak4_jet0_eta, ev.ak4_jet0_phi, ev.ak4_jet1_pt, ev.ak4_jet1_eta, ev.ak4_jet1_phi)
        #mt2.setMet((ev.met_px * ev.met_px + ev.met_py * ev.met_py)**(0.5), np.arctan(ev.met_py/ev.met_px))

        #mt2_ll_list.append(mt2.mt2ll())
        #mt2_bb_list.append(mt2.mt2bb())
        #mt2_blbl_list.append(mt2.mt2blbl())
        #mt2.reset()

        """
        lep0_p4 = ROOT.Math.LorentzVector('ROOT::Math::PtEtaPhiE4D<double>')(ev.lep0_pt, ev.lep0_eta, ev.lep0_phi, ev.lep0_E)
        lep1_p4 = ROOT.Math.LorentzVector('ROOT::Math::PtEtaPhiE4D<double>')(ev.lep1_pt, ev.lep1_eta, ev.lep1_phi, ev.lep1_E)
        bjet0_p4 = ROOT.Math.LorentzVector('ROOT::Math::PtEtaPhiE4D<double>')(ev.ak4_jet0_pt, ev.ak4_jet0_eta, ev.ak4_jet0_phi, ev.ak4_jet0_E)
        bjet1_p4 = ROOT.Math.LorentzVector('ROOT::Math::PtEtaPhiE4D<double>')(ev.ak4_jet1_pt, ev.ak4_jet1_eta, ev.ak4_jet1_phi, ev.ak4_jet1_E)
        if ev.Double_HbbFat:
            bjet0_p4 = ROOT.Math.LorentzVector('ROOT::Math::PtEtaPhiE4D<double>')(ev.ak8_jet0_subjet1_pt, ev.ak8_jet0_subjet1_eta, ev.ak8_jet0_subjet1_phi, ev.ak8_jet0_subjet1_E)
            bjet1_p4 = ROOT.Math.LorentzVector('ROOT::Math::PtEtaPhiE4D<double>')(ev.ak8_jet0_subjet2_pt, ev.ak8_jet0_subjet2_eta, ev.ak8_jet0_subjet2_phi, ev.ak8_jet0_subjet2_E)
        """


        lep0_p4 = vector.obj(pt = ev.lep0_pt, eta = ev.lep0_eta, phi = ev.lep0_phi, energy = ev.lep0_E)
        lep1_p4 = vector.obj(pt = ev.lep1_pt, eta = ev.lep1_eta, phi = ev.lep1_phi, energy = ev.lep1_E)
        bjet0_p4 = vector.obj(pt = ev.ak4_jet0_pt, eta = ev.ak4_jet0_eta, phi = ev.ak4_jet0_phi, energy = ev.ak4_jet0_E)
        bjet1_p4 = vector.obj(pt = ev.ak4_jet0_pt, eta = ev.ak4_jet0_eta, phi = ev.ak4_jet0_phi, energy = ev.ak4_jet0_E)
        if ev.Double_HbbFat:
            bjet0_p4 = vector.obj(pt = ev.ak8_jet0_subjet1_pt, eta = ev.ak8_jet0_subjet1_eta, phi = ev.ak8_jet0_subjet1_phi, energy = ev.ak8_jet0_subjet1_E)
            bjet1_p4 = vector.obj(pt = ev.ak8_jet0_subjet2_pt, eta = ev.ak8_jet0_subjet2_eta, phi = ev.ak8_jet0_subjet2_phi, energy = ev.ak8_jet0_subjet2_E)


        mt2_ll_lester_list.append(mt2_lester.get_mT2(lep0_p4.M, lep0_p4.px, lep0_p4.py, lep1_p4.M, lep1_p4.px, lep1_p4.py, ev.met_px+bjet0_p4.px+bjet1_p4.px, ev.met_py+bjet0_p4.py+bjet1_p4.py, bjet0_p4.M, bjet1_p4.M, 0, True))
        wMass = 80.4
        mt2_bb_lester_list.append(mt2_lester.get_mT2(bjet0_p4.M, bjet0_p4.px, bjet0_p4.py, bjet1_p4.M, bjet1_p4.px, bjet1_p4.py, ev.met_px+lep0_p4.px+lep1_p4.px, ev.met_py+lep0_p4.py+lep1_p4.py, wMass, wMass, 0, True))

        lep0bjet0_p4 = lep0_p4 + bjet0_p4
        lep0bjet1_p4 = lep0_p4 + bjet1_p4
        lep1bjet0_p4 = lep1_p4 + bjet0_p4
        lep1bjet1_p4 = lep1_p4 + bjet1_p4
        if max(lep0bjet0_p4.M, lep1bjet1_p4.M) < max(lep0bjet1_p4.M, lep1bjet0_p4.M):
            mt2_blbl_lester_list.append(mt2_lester.get_mT2(lep0bjet0_p4.M, lep0bjet0_p4.px, lep0bjet0_p4.py, lep1bjet1_p4.M, lep1bjet1_p4.px, lep1bjet1_p4.py, ev.met_px, ev.met_py, 0, 0, 0, True))
        else:
            mt2_blbl_lester_list.append(mt2_lester.get_mT2(lep0bjet1_p4.M, lep0bjet1_p4.px, lep0bjet1_p4.py, lep1bjet0_p4.M, lep1bjet0_p4.px, lep1bjet0_p4.py, ev.met_px, ev.met_py, 0, 0, 0, True))


    #events['mt2_ll'] = mt2_ll_list
    #events['mt2_bb'] = mt2_bb_list
    #events['mt2_blbl'] = mt2_blbl_list

    events['mt2_ll_lester'] = mt2_ll_lester_list
    events['mt2_bb_lester'] = mt2_bb_lester_list
    events['mt2_blbl_lester'] = mt2_blbl_lester_list

    syst = (key.split("Tree")[0]).split("Double_HME_")[1]
    outfile['Double_HME_MT2_'+syst+'Tree'] = events

#For now, we don't need to save single trees
if save_single:
    if 'Single_HME_Tree' in '\t'.join(f.keys()):
        outfile['Single_HME_MT2_Tree'] = (f['Single_HME_Tree']).arrays()

outfile['nEvents'] = (f['nEvents']).arrays()
