# HHbbWW_Run3

Package for TAMU analysis of H -> hh -> bbWW

Instructions to use directly with an example file

If on lxplus
```
source /cvmfs/sft.cern.ch/lcg/views/LCG_104/x86_64-el9-gcc13-opt/setup.sh
```

Then
```
python3 python/run_bbWW_processing.py -i /afs/cern.ch/user/d/daebi/public/bbWW_example_files/bbWW_testfile_radion2LM450.root -o bbWW_step1.root -ry 2022 -MC 1
python3 hme/run_hme.py -i bbWW_step1.root -o bbWW_step2.root -it 10000
python3 mt2/calculate_mt2.py -i bbWW_step2.root -o bbWW_step3.root
python3 dnn/run_dnn.py -i bbWW_step3.root -o bbWW_step4.root -m dnn/DNN_Model_Example/2022_preEE/TT_ST_DY_signal
```

Or on lxplus, xrootd is supported to avoid downloading the inputFile. To initialize the grid access and source the needed python packages follow
```
voms-proxy-init --rfc --voms cms -valid 192:00
source /cvmfs/sft.cern.ch/lcg/views/LCG_104/x86_64-el9-gcc13-opt/setup.sh
python3 run_bbWW_processing.py -i root://cmsxrootd.fnal.gov//path/to/dataset/file.root -o Output_File
```
From here, you can continue the other steps

To use through condor, look at the README in the condor directory

All major pieces of the analysis are handled by separate folders

Major pieces are:
- NanoAOD to event selected flat nTuple -- ```python/run_bbWW_processing.py```
- Add HME values to tree -- ```hme/run_hme.py```
- Add MT2 values to tree -- ```mt2/calculate_mt2.py```
- Add DNN output to tree -- ```dnn/run_dnn.py```


Main processing steps are:
- Scale Factors -- python/corrections.py
- GenParticle Matching -- python/genparticles.py
- Object Selection -- python/object_selection.py
- Event Selection -- python/event_selection.py
- Tree Creation -- python/tree_manager.py
