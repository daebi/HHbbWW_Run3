#!/bin/bash

list_of_files=("test0.root" "test1.root")
filename=("filename.sh")
runyear=("2022")
isMC=("1")
XS=("1.0")
DNN_Val=("-1")
SF=("0")
HLTCut=("1")
useXrootD=("1")
PYTHON_FOLDER=("/afs/cern.ch/work/d/daebi/diHiggs/HHbbWW_Run3/python/")

HME_FOLDER=("/afs/cern.ch/work/d/daebi/diHiggs/HHbbWW_Run3/hme")
iterations=("1000")
singleHME=("0")
doubleHME=("0")

doMT2=("0")
MT2_FOLDER=("/afs/cern.ch/work/d/daebi/diHiggs/HHbbWW_Run3/mt2")

doDNN=("0")
DNN_FOLDER=("/afs/cern.ch/work/d/daebi/diHiggs/HHbbWW_Run3/dnn")

doSL=("0")
doDL=("0")

source /cvmfs/sft.cern.ch/lcg/views/LCG_104/x86_64-el9-gcc13-opt/setup.sh

ls -lh

if [ $useXrootD -eq "1" ]
then
  python3 ${PYTHON_FOLDER}/run_bbWW_processing.py -i ${list_of_files} -o out_by_hand_${filename}.root -d 0 -ry ${runyear} -MC ${isMC} -XS ${XS} -t ${DNN_Val} -SF ${SF} -HLTCut ${HLTCut} -doSL ${doSL} -doDL ${doDL}
else
  filecount=0
  list_of_tmp_files=("")
  for in_filename in $list_of_files
  do
    xrdcp $in_filename tmp_file${filecount}.root
    list_of_tmp_files+=" tmp_file"${filecount}.root
    filecount=$((filecount+1))
  done
  python3 ${PYTHON_FOLDER}/run_bbWW_processing.py -i ${list_of_tmp_files} -o out_by_hand_${filename}.root -d 0 -ry ${runyear} -MC ${isMC} -XS ${XS} -t ${DNN_Val} -SF ${SF} -HLTCut ${HLTCut} -doSL ${doSL} -doDL ${doDL}
  rm tmp_file*
fi

ls -lh

echo 'Starting HME'

python3 ${HME_FOLDER}/run_hme.py -i out_by_hand_${filename}.root -o out_by_hand_hme_${filename}.root -d 0 -it ${iterations} -SL ${singleHME} -DL ${doubleHME}

ls -lh

if [ $doMT2 -eq "1" ]
then
  python3 ${MT2_FOLDER}/calculate_mt2.py -i out_by_hand_hme_${filename}.root -o out_by_hand_hme_mt2_${filename}.root
fi

ls -lh

if [ $doDNN -eq "1" ]
then
  python3 ${DNN_FOLDER}/run_dnn.py -i out_by_hand_hme_mt2_${filename}.root -o out_by_hand_hme_mt2_dnn_${filename}.root -m ${DNN_FOLDER}/DNN_Model_Example/2022_MultiClass/signal_tt_dy_st_other
fi

echo 'Done!'
