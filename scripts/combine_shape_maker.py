import ROOT
import os
import uproot
import awkward as ak
import numpy as np
import hist

class CombineShapeMaker():
    def __init__(self):
        print("Init")
        self.lum = 7980.4
        self.lum = 138000
        self.blind = True

    def prepare_dataset(self, dataset_path, outname):
        if ("/data" in dataset_path) and (self.blind):
            print("You are blinded! Skipping Data")
            return
        outfile = uproot.recreate(outname)
        nEvents_in_dataset = 0
        sumGenWeight_in_dataset = 0
        fname_list = []
        if "/data" in  dataset_path:
            print("looking at data ", dataset_path)
            fname_list = [dataset_path+'/'+fname for fname in os.listdir(dataset_path) if ('.root' in fname) and ('NoDuplicates' in fname)]
            print(fname_list)
            for fname in fname_list:
                f = uproot.open(fname)
                if 'Double_HME_MT2_DNN_Tree' not in '\t'.join(f.keys()): continue
                events = f['Double_HME_MT2_DNN_Tree'].arrays()
                #Data doesn't have nEvents scaling!
                #nEvents = f['nEvents'].arrays()
                #nEvents_in_dataset += np.sum(nEvents.nEvents)
                #sumGenWeight_in_dataset += np.sum(nEvents.sumGenWeight)

                events_ee_res2b = events[(events.Double_Signal == 1) & (events.double_is_ee == 1) & (events.Double_Res_2b == 1)]
                events_ee_res1b = events[(events.Double_Signal == 1) & (events.double_is_ee == 1) & (events.Double_Res_1b == 1)]
                events_ee_boost = events[(events.Double_Signal == 1) & (events.double_is_ee == 1) & (events.Double_HbbFat == 1)]

                events_emu_res2b = events[(events.Double_Signal == 1) & ((events.double_is_em == 1) | (events.double_is_me == 1)) & (events.Double_Res_2b == 1)]
                events_emu_res1b = events[(events.Double_Signal == 1) & ((events.double_is_em == 1) | (events.double_is_me == 1)) & (events.Double_Res_1b == 1)]
                events_emu_boost = events[(events.Double_Signal == 1) & ((events.double_is_em == 1) | (events.double_is_me == 1)) & (events.Double_HbbFat == 1)]

                events_mumu_res2b = events[(events.Double_Signal == 1) & (events.double_is_mm == 1) & (events.Double_Res_2b == 1)]
                events_mumu_res1b = events[(events.Double_Signal == 1) & (events.double_is_mm == 1) & (events.Double_Res_1b == 1)]
                events_mumu_boost = events[(events.Double_Signal == 1) & (events.double_is_mm == 1) & (events.Double_HbbFat == 1)]

                events_ee_res2b = self.drop_branches(events_ee_res2b)
                events_ee_res1b = self.drop_branches(events_ee_res1b)
                events_ee_boost = self.drop_branches(events_ee_boost)

                events_emu_res2b = self.drop_branches(events_emu_res2b)
                events_emu_res1b = self.drop_branches(events_emu_res1b)
                events_emu_boost = self.drop_branches(events_emu_boost)

                events_mumu_res2b = self.drop_branches(events_mumu_res2b)
                events_mumu_res1b = self.drop_branches(events_mumu_res1b)
                events_mumu_boost = self.drop_branches(events_mumu_boost)

                if 'ee/res2b' in '\t'.join(outfile.keys()): outfile['ee/res2b'].extend(events_ee_res2b)
                else: outfile['ee/res2b'] = events_ee_res2b
                if 'ee/res1b' in '\t'.join(outfile.keys()): outfile['ee/res1b'].extend(events_ee_res1b)
                else: outfile['ee/res1b'] = events_ee_res1b
                if 'ee/boost' in '\t'.join(outfile.keys()): outfile['ee/boost'].extend(events_ee_boost)
                else: outfile['ee/boost'] = events_ee_boost

                if 'emu/res2b' in '\t'.join(outfile.keys()): outfile['emu/res2b'].extend(events_emu_res2b)
                else: outfile['emu/res2b'] = events_emu_res2b
                if 'emu/res1b' in '\t'.join(outfile.keys()): outfile['emu/res1b'].extend(events_emu_res1b)
                else: outfile['emu/res1b'] = events_emu_res1b
                if 'emu/boost' in '\t'.join(outfile.keys()): outfile['emu/boost'].extend(events_emu_boost)
                else: outfile['emu/boost'] = events_emu_boost

                if 'mumu/res2b' in '\t'.join(outfile.keys()): outfile['mumu/res2b'].extend(events_mumu_res2b)
                else: outfile['mumu/res2b'] = events_mumu_res2b
                if 'mumu/res1b' in '\t'.join(outfile.keys()): outfile['mumu/res1b'].extend(events_mumu_res1b)
                else: outfile['mumu/res1b'] = events_mumu_res1b
                if 'mumu/boost' in '\t'.join(outfile.keys()): outfile['mumu/boost'].extend(events_mumu_boost)
                else: outfile['mumu/boost'] = events_mumu_boost

                f.close()
        else:
            for subdir in os.listdir(dataset_path):
                print("Looking at subdir ", subdir)
                fname_list = [dataset_path+subdir+'/'+fname for fname in os.listdir(dataset_path+subdir) if '.root' in fname]
                for fname in fname_list:
                    f = uproot.open(fname)
                    if 'Double_HME_MT2_DNN_Tree' not in '\t'.join(f.keys()):
                        f.close()
                        continue
                    events = f['Double_HME_MT2_DNN_Tree'].arrays()
                    nEvents = f['nEvents'].arrays()
                    nEvents_in_dataset += np.sum(nEvents.nEvents)
                    sumGenWeight_in_dataset += np.sum(nEvents.sumGenWeight)

                    events_ee_res2b = events[(events.Double_Signal == 1) & (events.double_is_ee == 1) & (events.Double_Res_2b == 1)]
                    events_ee_res1b = events[(events.Double_Signal == 1) & (events.double_is_ee == 1) & (events.Double_Res_1b == 1)]
                    events_ee_boost = events[(events.Double_Signal == 1) & (events.double_is_ee == 1) & (events.Double_HbbFat == 1)]

                    events_emu_res2b = events[(events.Double_Signal == 1) & ((events.double_is_em == 1) | (events.double_is_me == 1)) & (events.Double_Res_2b == 1)]
                    events_emu_res1b = events[(events.Double_Signal == 1) & ((events.double_is_em == 1) | (events.double_is_me == 1)) & (events.Double_Res_1b == 1)]
                    events_emu_boost = events[(events.Double_Signal == 1) & ((events.double_is_em == 1) | (events.double_is_me == 1)) & (events.Double_HbbFat == 1)]

                    events_mumu_res2b = events[(events.Double_Signal == 1) & (events.double_is_mm == 1) & (events.Double_Res_2b == 1)]
                    events_mumu_res1b = events[(events.Double_Signal == 1) & (events.double_is_mm == 1) & (events.Double_Res_1b == 1)]
                    events_mumu_boost = events[(events.Double_Signal == 1) & (events.double_is_mm == 1) & (events.Double_HbbFat == 1)]

                    events_ee_res2b = self.drop_branches(events_ee_res2b)
                    events_ee_res1b = self.drop_branches(events_ee_res1b)
                    events_ee_boost = self.drop_branches(events_ee_boost)

                    events_emu_res2b = self.drop_branches(events_emu_res2b)
                    events_emu_res1b = self.drop_branches(events_emu_res1b)
                    events_emu_boost = self.drop_branches(events_emu_boost)

                    events_mumu_res2b = self.drop_branches(events_mumu_res2b)
                    events_mumu_res1b = self.drop_branches(events_mumu_res1b)
                    events_mumu_boost = self.drop_branches(events_mumu_boost)

                    if 'ee/res2b' in '\t'.join(outfile.keys()): outfile['ee/res2b'].extend(events_ee_res2b)
                    else: outfile['ee/res2b'] = events_ee_res2b
                    if 'ee/res1b' in '\t'.join(outfile.keys()): outfile['ee/res1b'].extend(events_ee_res1b)
                    else: outfile['ee/res1b'] = events_ee_res1b
                    if 'ee/boost' in '\t'.join(outfile.keys()): outfile['ee/boost'].extend(events_ee_boost)
                    else: outfile['ee/boost'] = events_ee_boost

                    if 'emu/res2b' in '\t'.join(outfile.keys()): outfile['emu/res2b'].extend(events_emu_res2b)
                    else: outfile['emu/res2b'] = events_emu_res2b
                    if 'emu/res1b' in '\t'.join(outfile.keys()): outfile['emu/res1b'].extend(events_emu_res1b)
                    else: outfile['emu/res1b'] = events_emu_res1b
                    if 'emu/boost' in '\t'.join(outfile.keys()): outfile['emu/boost'].extend(events_emu_boost)
                    else: outfile['emu/boost'] = events_emu_boost

                    if 'mumu/res2b' in '\t'.join(outfile.keys()): outfile['mumu/res2b'].extend(events_mumu_res2b)
                    else: outfile['mumu/res2b'] = events_mumu_res2b
                    if 'mumu/res1b' in '\t'.join(outfile.keys()): outfile['mumu/res1b'].extend(events_mumu_res1b)
                    else: outfile['mumu/res1b'] = events_mumu_res1b
                    if 'mumu/boost' in '\t'.join(outfile.keys()): outfile['mumu/boost'].extend(events_mumu_boost)
                    else: outfile['mumu/boost'] = events_mumu_boost


                    jerjes_names = ["Double_HME_MT2_DNN_jer_up_Tree", "Double_HME_MT2_DNN_jer_down_Tree", "Double_HME_MT2_DNN_jes_up_Tree", "Double_HME_MT2_DNN_jes_down_Tree"]
                    for jerjes_name in jerjes_names:
                        if jerjes_name not in '\t'.join(f.keys()):
                            continue
                        events_jerjes = f[jerjes_name].arrays()
                        events_jerjes_ee_res2b = events_jerjes[(events_jerjes.Double_Signal == 1) & (events_jerjes.double_is_ee == 1) & (events_jerjes.Double_Res_2b == 1)]
                        events_jerjes_ee_res1b = events_jerjes[(events_jerjes.Double_Signal == 1) & (events_jerjes.double_is_ee == 1) & (events_jerjes.Double_Res_1b == 1)]
                        events_jerjes_ee_boost = events_jerjes[(events_jerjes.Double_Signal == 1) & (events_jerjes.double_is_ee == 1) & (events_jerjes.Double_HbbFat == 1)]

                        events_jerjes_emu_res2b = events_jerjes[(events_jerjes.Double_Signal == 1) & ((events_jerjes.double_is_em == 1) | (events_jerjes.double_is_me == 1)) & (events_jerjes.Double_Res_2b == 1)]
                        events_jerjes_emu_res1b = events_jerjes[(events_jerjes.Double_Signal == 1) & ((events_jerjes.double_is_em == 1) | (events_jerjes.double_is_me == 1)) & (events_jerjes.Double_Res_1b == 1)]
                        events_jerjes_emu_boost = events_jerjes[(events_jerjes.Double_Signal == 1) & ((events_jerjes.double_is_em == 1) | (events_jerjes.double_is_me == 1)) & (events_jerjes.Double_HbbFat == 1)]

                        events_jerjes_mumu_res2b = events_jerjes[(events_jerjes.Double_Signal == 1) & (events_jerjes.double_is_mm == 1) & (events_jerjes.Double_Res_2b == 1)]
                        events_jerjes_mumu_res1b = events_jerjes[(events_jerjes.Double_Signal == 1) & (events_jerjes.double_is_mm == 1) & (events_jerjes.Double_Res_1b == 1)]
                        events_jerjes_mumu_boost = events_jerjes[(events_jerjes.Double_Signal == 1) & (events_jerjes.double_is_mm == 1) & (events_jerjes.Double_HbbFat == 1)]

                        events_jerjes_ee_res2b = self.drop_branches(events_jerjes_ee_res2b)
                        events_jerjes_ee_res1b = self.drop_branches(events_jerjes_ee_res1b)
                        events_jerjes_ee_boost = self.drop_branches(events_jerjes_ee_boost)

                        events_jerjes_emu_res2b = self.drop_branches(events_jerjes_emu_res2b)
                        events_jerjes_emu_res1b = self.drop_branches(events_jerjes_emu_res1b)
                        events_jerjes_emu_boost = self.drop_branches(events_jerjes_emu_boost)

                        events_jerjes_mumu_res2b = self.drop_branches(events_jerjes_mumu_res2b)
                        events_jerjes_mumu_res1b = self.drop_branches(events_jerjes_mumu_res1b)
                        events_jerjes_mumu_boost = self.drop_branches(events_jerjes_mumu_boost)

                        jerjes_shortname = list("".join(jerjes_name.split('_')[4:6]))
                        jerjes_shortname[3] = jerjes_shortname[3].upper()
                        jerjes_shortname = "".join(jerjes_shortname)
                        if 'ee/res2b_'+jerjes_shortname in '\t'.join(outfile.keys()): outfile['ee/res2b_'+jerjes_shortname].extend(events_jerjes_ee_res2b)
                        else: outfile['ee/res2b_'+jerjes_shortname] = events_jerjes_ee_res2b
                        if 'ee/res1b_'+jerjes_shortname in '\t'.join(outfile.keys()): outfile['ee/res1b_'+jerjes_shortname].extend(events_jerjes_ee_res1b)
                        else: outfile['ee/res1b_'+jerjes_shortname] = events_jerjes_ee_res1b
                        if 'ee/boost_'+jerjes_shortname in '\t'.join(outfile.keys()): outfile['ee/boost_'+jerjes_shortname].extend(events_jerjes_ee_boost)
                        else: outfile['ee/boost_'+jerjes_shortname] = events_jerjes_ee_boost

                        if 'emu/res2b_'+jerjes_shortname in '\t'.join(outfile.keys()): outfile['emu/res2b_'+jerjes_shortname].extend(events_jerjes_emu_res2b)
                        else: outfile['emu/res2b_'+jerjes_shortname] = events_jerjes_emu_res2b
                        if 'emu/res1b_'+jerjes_shortname in '\t'.join(outfile.keys()): outfile['emu/res1b_'+jerjes_shortname].extend(events_jerjes_emu_res1b)
                        else: outfile['emu/res1b_'+jerjes_shortname] = events_jerjes_emu_res1b
                        if 'emu/boost_'+jerjes_shortname in '\t'.join(outfile.keys()): outfile['emu/boost_'+jerjes_shortname].extend(events_jerjes_emu_boost)
                        else: outfile['emu/boost_'+jerjes_shortname] = events_jerjes_emu_boost

                        if 'mumu/res2b_'+jerjes_shortname in '\t'.join(outfile.keys()): outfile['mumu/res2b_'+jerjes_shortname].extend(events_jerjes_mumu_res2b)
                        else: outfile['mumu/res2b_'+jerjes_shortname] = events_jerjes_mumu_res2b
                        if 'mumu/res1b_'+jerjes_shortname in '\t'.join(outfile.keys()): outfile['mumu/res1b_'+jerjes_shortname].extend(events_jerjes_mumu_res1b)
                        else: outfile['mumu/res1b_'+jerjes_shortname] = events_jerjes_mumu_res1b
                        if 'mumu/boost_'+jerjes_shortname in '\t'.join(outfile.keys()): outfile['mumu/boost_'+jerjes_shortname].extend(events_jerjes_mumu_boost)
                        else: outfile['mumu/boost_'+jerjes_shortname] = events_jerjes_mumu_boost

                    f.close()
        print('nEvts ', nEvents_in_dataset)
        print('genWeight ', sumGenWeight_in_dataset)
        outfile['nEvents'] = np.histogram(0, weights=nEvents_in_dataset)
        outfile['sumGenWeight'] = np.histogram(0, weights=sumGenWeight_in_dataset)
        outfile.close()

        #outfile['nEvents'] = {'nEvents': nEvents_in_dataset, 'sumGenWeight': sumGenWeight_in_dataset}

    def drop_branches(self, events):
        new_events = {}
        keep_list = [
            'DNN_Signal_m250', 'DNN_Signal_m260', 'DNN_Signal_m270', 'DNN_Signal_m280', 'DNN_Signal_m300', 'DNN_Signal_m350', 'DNN_Signal_m450', 'DNN_Signal_m550', 'DNN_Signal_m600', 'DNN_Signal_m650', 'DNN_Signal_m700', 'DNN_Signal_m800',
            'Double_HbbFat', 'Double_Res_1b', 'Double_Res_2b', 'Double_Signal', 'Double_Fake', 'double_is_ee', 'double_is_mm', 'double_is_em', 'double_is_me',
            'tt_reweight',
            'pu_reweight', 'pu_reweight_up', 'pu_reweight_down',
            'XS',
            'genWeight',
            'double_event_weight',
            'lep0_lepton_ID_SF', 'lep0_lepton_ID_SF_up', 'lep0_lepton_ID_SF_down',
            'lep1_lepton_ID_SF', 'lep1_lepton_ID_SF_up', 'lep1_lepton_ID_SF_down',
            'ak4_jet0_btag_SF', 'ak4_jet0_btag_SF_up', 'ak4_jet0_btag_SF_down',
            'ak4_jet1_btag_SF', 'ak4_jet1_btag_SF_up', 'ak4_jet1_btag_SF_down',
            #Some simple variables for Data/MC comparison
            'hbb_mass',
            'lep0_pt', 'lep0_eta', 'lep0_phi',
            'lep1_pt', 'lep1_eta', 'lep1_phi',
            'ak4_jet0_pt', 'ak4_jet0_eta', 'ak4_jet0_phi',
            'ak4_jet1_pt', 'ak4_jet1_eta', 'ak4_jet1_phi'
        ]
        for branch in keep_list:
            new_events[branch] = events[branch]
        return new_events

    def prepare_all_datasets(self, storage_dir, out_dir):
        os.makedirs(out_dir, exist_ok = True)

        #data_names = ['DoubleMuon', 'EGamma', 'Muon', 'MuonEG', 'SingleMuon', 'data']
        skip_names = ['DoubleMuon', 'EGamma', 'Muon', 'MuonEG', 'SingleMuon']
        #data_names = ['data']
        dataset_list = [dataset for dataset in os.listdir(storage_dir) if (dataset not in skip_names)]

        for dataset in dataset_list:
            print("Looking at dataset ", dataset)
            self.prepare_dataset(storage_dir+dataset+"/", out_dir+dataset+"_SmallForCombine.root")


    def make_process_dict(self, dir):
        process_dict = {}

        all_files = os.listdir(dir)
        process_dict['data_obs'] = [fname for fname in all_files if ("data" in fname)]
        all_files = [fname for fname in all_files if ("data" not in fname)]
        process_dict['Signal'] = [fname for fname in all_files if (("Radion" in fname) or ("Graviton" in fname))]
        all_files = [fname for fname in all_files if (("Radion" not in fname) and ("Graviton" not in fname))]
        process_dict['TT'] = [fname for fname in all_files if ("TT" in fname)]
        all_files = [fname for fname in all_files if ("TT" not in fname)]
        process_dict['DY'] = [fname for fname in all_files if ("DY" in fname)]
        all_files = [fname for fname in all_files if ("DY" not in fname)]
        process_dict['ST'] = [fname for fname in all_files if (("Tbar" in fname) or ("TW" in fname))]
        all_files = [fname for fname in all_files if (("Tbar" not in fname) and ("TW" not in fname))]
        process_dict['VV'] = [fname for fname in all_files if (("WW" in fname) or ("WZ" in fname) or ("ZZ" in fname))]
        all_files = [fname for fname in all_files if (("WW" not in fname) and ("WZ" not in fname) and ("ZZ" not in fname))]
        process_dict['Other'] = [fname for fname in all_files]

        return process_dict

    def create_shape_file(self, process_dict, dir, shape_output_dir):
        os.makedirs(shape_output_dir, exist_ok = True)
        for m in [250, 260, 270, 280, 300, 350, 450, 550, 600, 650, 700, 800]:
            print("Looking at mass ", m)
            outfile = uproot.recreate(shape_output_dir+"shape_m{}.root".format(m))


            for key in process_dict.keys():
                print("Looking at dict key ", key)
                if 'data_obs' == key: continue #Don't look at data yet
                dnn_out_dict = {}
                weights_dict = {}

                #JerJes are stored in separate trees, need new set of variables
                dnn_out_jerjes = {}
                weights_jerjes = {}

                for channel in ['ee', 'emu', 'mumu']:
                        for category in ['res2b', 'res1b', 'boost']:
                            dnn_out_dict[channel+'_'+category] = []
                            weights_dict[channel+'_'+category] = {}

                            for jerjes in ['jerUp', 'jerDown', 'jesUp', 'jesDown']:
                                dnn_out_jerjes[channel+'_'+category+'_'+jerjes] = []
                                weights_jerjes[channel+'_'+category+'_'+jerjes] = {}

                
                

                dnn_out_ee_res2b, dnn_out_ee_res1b, dnn_out_ee_boost = [], [], []
                weights_ee_res2b_dict, weights_ee_res1b_dict, weights_ee_boost_dict = {}, {}, {}

                dnn_out_emu_res2b, dnn_out_emu_res1b, dnn_out_emu_boost = [], [], []
                weights_emu_res2b_dict, weights_emu_res1b_dict, weights_emu_boost_dict = {}, {}, {}

                dnn_out_mumu_res2b, dnn_out_mumu_res1b, dnn_out_mumu_boost = [], [], []
                weights_mumu_res2b_dict, weights_mumu_res1b_dict, weights_mumu_boost_dict = {}, {}, {}
                print("Files in key are ", process_dict[key])
                for fname in process_dict[key]:
                    if ('Signal' == key) or ('ggRadion_HH_bbWW_M{}'.format(m) == key): #Add second key for the rename
                        if 'M-{}_'.format(m) not in fname:
                            continue
                        if 'Radion' not in fname:
                            continue #For now we will only do Radion
                        key = 'ggRadion_HH_bbWW_M{}'.format(m)
                    if 'data_obs' == key:
                        if 'NoDuplicates' not in fname:
                            continue
                    print("Looking at file ", fname)
                    f = uproot.open(dir+fname)
                    sumGenWeight = np.sum(f['sumGenWeight'].values())
                    #Quick fix to have data not scaled by lum
                    tmp_lumi_bank = self.lum
                    if key == 'data_obs': self.lum = 1.0
                    for channel in ['ee', 'emu', 'mumu']:
                        for category in ['res2b', 'res1b', 'boost']:
                            events = f[channel+'/'+category].arrays()
                            weights_dict[channel+"_"+category] = self.res_event_weight(weights_dict[channel+"_"+category], events, sumGenWeight)
                            dnn_out_dict[channel+"_"+category] = np.concatenate((dnn_out_dict[channel+"_"+category], events['DNN_Signal_m{}'.format(m)]))

                            for jerjes in ['jerUp', 'jerDown', 'jesUp', 'jesDown']:
                                events_jerjes = f[channel+'/'+category+'_'+jerjes].arrays()
                                weights_jerjes[channel+'_'+category+'_'+jerjes] = self.res_event_weight(weights_jerjes[channel+'_'+category+'_'+jerjes], events_jerjes, sumGenWeight)
                                dnn_out_jerjes[channel+'_'+category+'_'+jerjes] = np.concatenate((dnn_out_jerjes[channel+'_'+category+'_'+jerjes], events_jerjes['DNN_Signal_m{}'.format(m)]))

                    if key == 'data_obs': self.lum = tmp_lumi_bank


                nBins = 100
                lims=(0.0,1.0)
                uncert_list = ['nom', 'puUp', 'puDown', 'eleIDUp', 'eleIDDown', 'muIDUp', 'muIDDown']
                for uncert in uncert_list:
                    if (key == 'data_obs') and (uncert != 'nom'): continue
                    dictname = key+"_"+uncert
                    if uncert == 'nom': dictname = key

                    for channel in ['ee', 'emu', 'mumu']:
                        for category in ['res2b', 'res1b', 'boost']:
                            outfile[channel+'/'+category+'/'+dictname] = (hist.Hist.new.Reg(nBins, lims[0], lims[1]).Weight()).fill(dnn_out_dict[channel+'_'+category], weight=weights_dict[channel+'_'+category][uncert])

                for jerjes_uncert in ['jerUp', 'jerDown', 'jesUp', 'jesDown']:
                    if (key == 'data_obs'): continue
                    dictname = key+"_"+jerjes_uncert
                    
                    for channel in ['ee', 'emu', 'mumu']:
                        for category in ['res2b', 'res1b', 'boost']:
                            outfile[channel+'/'+category+'/'+dictname] = (hist.Hist.new.Reg(nBins, lims[0], lims[1]).Weight()).fill(dnn_out_jerjes[channel+'_'+category+'_'+jerjes_uncert], weight=weights_jerjes[channel+'_'+category+'_'+jerjes_uncert][uncert])



    def res_event_weight(self, res_weight_dict, events, sumGenWeight):
        if len(res_weight_dict) == 0:
            print("Dict is empty, initializing")
            res_weight_dict['nom'] = []
            res_weight_dict['puUp'] = []
            res_weight_dict['puDown'] = []
            res_weight_dict['eleIDUp'] = []
            res_weight_dict['eleIDDown'] = []
            res_weight_dict['muIDUp'] = []
            res_weight_dict['muIDDown'] = []
            res_weight_dict['jet0Btag_up'] = []
            res_weight_dict['jet0Btag_down'] = []
            res_weight_dict['jet1Btag_up'] = []
            res_weight_dict['jet1Btag_down'] = []

        lep_ID_SF = events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF
        ele_ID_up = np.where(events.double_is_ee,
            events.lep0_lepton_ID_SF_up * events.lep1_lepton_ID_SF_up,
            np.where(events.double_is_em,
                events.lep0_lepton_ID_SF_up * events.lep1_lepton_ID_SF,
                np.where(events.double_is_me,
                    events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF_up,
                    lep_ID_SF
                )
            )
        )
        ele_ID_down = np.where(events.double_is_ee,
            events.lep0_lepton_ID_SF_down * events.lep1_lepton_ID_SF_down,
            np.where(events.double_is_em,
                events.lep0_lepton_ID_SF_down * events.lep1_lepton_ID_SF,
                np.where(events.double_is_me,
                    events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF_down,
                    lep_ID_SF
                )
            )
        )
        mu_ID_up = np.where(events.double_is_mm,
            events.lep0_lepton_ID_SF_up * events.lep1_lepton_ID_SF_up,
            np.where(events.double_is_me,
                events.lep0_lepton_ID_SF_up * events.lep1_lepton_ID_SF,
                np.where(events.double_is_em,
                    events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF_up,
                    lep_ID_SF
                )
            )
        )
        mu_ID_down = np.where(events.double_is_mm,
            events.lep0_lepton_ID_SF_down * events.lep1_lepton_ID_SF_down,
            np.where(events.double_is_me,
                events.lep0_lepton_ID_SF_down * events.lep1_lepton_ID_SF,
                np.where(events.double_is_em,
                    events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF_down,
                    lep_ID_SF
                )
            )
        )
        res_weight_dict['nom'] = np.concatenate((res_weight_dict['nom'], events.tt_reweight * events.pu_reweight * events.genWeight * lep_ID_SF * events.ak4_jet0_btag_SF * events.ak4_jet1_btag_SF * events.XS * self.lum / sumGenWeight))
        res_weight_dict['puUp'] = np.concatenate((res_weight_dict['puUp'], events.tt_reweight * events.pu_reweight_up * events.genWeight * lep_ID_SF * events.ak4_jet0_btag_SF * events.ak4_jet1_btag_SF * events.XS * self.lum / sumGenWeight))
        res_weight_dict['puDown'] = np.concatenate((res_weight_dict['puDown'], events.tt_reweight * events.pu_reweight_down * events.genWeight * lep_ID_SF * events.ak4_jet0_btag_SF * events.ak4_jet1_btag_SF * events.XS * self.lum / sumGenWeight))
        res_weight_dict['eleIDUp'] = np.concatenate((res_weight_dict['eleIDUp'], events.tt_reweight * events.pu_reweight * events.genWeight * ele_ID_up * events.ak4_jet0_btag_SF * events.ak4_jet1_btag_SF * events.XS * self.lum / sumGenWeight))
        res_weight_dict['eleIDDown'] = np.concatenate((res_weight_dict['eleIDDown'], events.tt_reweight * events.pu_reweight * events.genWeight * ele_ID_down * events.ak4_jet0_btag_SF * events.ak4_jet1_btag_SF * events.XS * self.lum / sumGenWeight))
        res_weight_dict['muIDUp'] = np.concatenate((res_weight_dict['muIDUp'], events.tt_reweight * events.pu_reweight * events.genWeight * mu_ID_up * events.ak4_jet0_btag_SF * events.ak4_jet1_btag_SF * events.XS * self.lum / sumGenWeight))
        res_weight_dict['muIDDown'] = np.concatenate((res_weight_dict['muIDDown'], events.tt_reweight * events.pu_reweight * events.genWeight * mu_ID_down * events.ak4_jet0_btag_SF * events.ak4_jet1_btag_SF * events.XS * self.lum / sumGenWeight))
        #res_weight_dict['jet0Btag_up'] = np.concatenate((res_weight_dict['jet0Btag_up'], events.tt_reweight * events.pu_reweight * events.genWeight * events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF * events.ak4_jet0_btag_SF_up * events.ak4_jet1_btag_SF * events.XS * self.lum / sumGenWeight))
        #res_weight_dict['jet0Btag_down'] = np.concatenate((res_weight_dict['jet0Btag_down'], events.tt_reweight * events.pu_reweight * events.genWeight * events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF * events.ak4_jet0_btag_SF_down * events.ak4_jet1_btag_SF * events.XS * self.lum / sumGenWeight))
        #res_weight_dict['jet1Btag_up'] = np.concatenate((res_weight_dict['jet1Btag_up'], events.tt_reweight * events.pu_reweight * events.genWeight * events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF * events.ak4_jet0_btag_SF * events.ak4_jet1_btag_SF_up * events.XS * self.lum / sumGenWeight))
        #res_weight_dict['jet1Btag_down'] = np.concatenate((res_weight_dict['jet1Btag_down'], events.tt_reweight * events.pu_reweight * events.genWeight * events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF * events.ak4_jet0_btag_SF * events.ak4_jet1_btag_SF_down * events.XS * self.lum / sumGenWeight))

        return res_weight_dict


    def boost_event_weight(self, boost_weight_dict, events, sumGenWeight):
        if len(boost_weight_dict) == 0:
            print("Dict is empty, initializing")
            boost_weight_dict['nom'] = []
            boost_weight_dict['puUp'] = []
            boost_weight_dict['puDown'] = []
            boost_weight_dict['eleIDUp'] = []
            boost_weight_dict['eleIDDown'] = []
            boost_weight_dict['muIDUp'] = []
            boost_weight_dict['muIDDown'] = []

        lep_ID_SF = events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF
        ele_ID_up = np.where(events.double_is_ee,
            events.lep0_lepton_ID_SF_up * events.lep1_lepton_ID_SF_up,
            np.where(events.double_is_em,
                events.lep0_lepton_ID_SF_up * events.lep1_lepton_ID_SF,
                np.where(events.double_is_me,
                    events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF_up,
                    lep_ID_SF
                )
            )
        )
        ele_ID_down = np.where(events.double_is_ee,
            events.lep0_lepton_ID_SF_down * events.lep1_lepton_ID_SF_down,
            np.where(events.double_is_em,
                events.lep0_lepton_ID_SF_down * events.lep1_lepton_ID_SF,
                np.where(events.double_is_me,
                    events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF_down,
                    lep_ID_SF
                )
            )
        )
        mu_ID_up = np.where(events.double_is_mm,
            events.lep0_lepton_ID_SF_up * events.lep1_lepton_ID_SF_up,
            np.where(events.double_is_me,
                events.lep0_lepton_ID_SF_up * events.lep1_lepton_ID_SF,
                np.where(events.double_is_em,
                    events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF_up,
                    lep_ID_SF
                )
            )
        )
        mu_ID_down = np.where(events.double_is_mm,
            events.lep0_lepton_ID_SF_down * events.lep1_lepton_ID_SF_down,
            np.where(events.double_is_me,
                events.lep0_lepton_ID_SF_down * events.lep1_lepton_ID_SF,
                np.where(events.double_is_em,
                    events.lep0_lepton_ID_SF * events.lep1_lepton_ID_SF_down,
                    lep_ID_SF
                )
            )
        )

        boost_weight_dict['nom'] = np.concatenate((boost_weight_dict['nom'], events.tt_reweight * events.pu_reweight * events.genWeight * lep_ID_SF * events.XS * self.lum / sumGenWeight))
        boost_weight_dict['puUp'] = np.concatenate((boost_weight_dict['puUp'], events.tt_reweight * events.pu_reweight_up * events.genWeight * lep_ID_SF * events.XS * self.lum / sumGenWeight))
        boost_weight_dict['puDown'] = np.concatenate((boost_weight_dict['puDown'], events.tt_reweight * events.pu_reweight_down * events.genWeight * lep_ID_SF * events.XS * self.lum / sumGenWeight))
        boost_weight_dict['eleIDUp'] = np.concatenate((boost_weight_dict['eleIDUp'], events.tt_reweight * events.pu_reweight * events.genWeight * ele_ID_up * events.XS * self.lum / sumGenWeight))
        boost_weight_dict['eleIDDown'] = np.concatenate((boost_weight_dict['eleIDDown'], events.tt_reweight * events.pu_reweight * events.genWeight * ele_ID_down * events.XS * self.lum / sumGenWeight))
        boost_weight_dict['muIDUp'] = np.concatenate((boost_weight_dict['muIDUp'], events.tt_reweight * events.pu_reweight * events.genWeight * mu_ID_up * events.XS * self.lum / sumGenWeight))
        boost_weight_dict['muIDDown'] = np.concatenate((boost_weight_dict['muIDDown'], events.tt_reweight * events.pu_reweight * events.genWeight * mu_ID_down * events.XS * self.lum / sumGenWeight))

        return boost_weight_dict



    def create_shape_file_blind(self, process_dict, dir, shape_output_dir, variable):
        #Shape files for blinded data, uses another variable instead of DNN Signal output
        os.makedirs(shape_output_dir, exist_ok = True)
        for m in [250, 260, 270, 280, 300, 350, 450, 550, 600, 650, 700, 800]:
            print("Looking at mass ", m)
            outfile = uproot.recreate(shape_output_dir+"shape_m{}.root".format(m))
            for key in process_dict.keys():
                print("Looking at dict key ", key)
                var_ee_res2b, var_ee_res1b, var_ee_boost = [], [], []
                weights_ee_res2b_dict, weights_ee_res1b_dict, weights_ee_boost_dict = {}, {}, {}

                var_emu_res2b, var_emu_res1b, var_emu_boost = [], [], []
                weights_emu_res2b_dict, weights_emu_res1b_dict, weights_emu_boost_dict = {}, {}, {}

                var_mumu_res2b, var_mumu_res1b, var_mumu_boost = [], [], []
                weights_mumu_res2b_dict, weights_mumu_res1b_dict, weights_mumu_boost_dict = {}, {}, {}
                print("Files in the key are ", process_dict[key])
                if len(process_dict[key]) == 0: continue
                for fname in process_dict[key]:
                    if ('Signal' == key) or ('ggRadion_HH_bbWW_M{}'.format(m) == key): #Add second key for the rename
                        if 'M-{}_'.format(m) not in fname:
                            continue
                        if 'Radion' not in fname:
                            continue #For now we will only do Radion
                        key = 'ggRadion_HH_bbWW_M{}'.format(m)

                    f = uproot.open(dir+fname)
                    sumGenWeight = np.sum(f['sumGenWeight'].values())
                    if key == "data_obs": sumGenWeight = 1.0
                    events_ee_res2b = f['ee/res2b'].arrays()
                    events_ee_res1b = f['ee/res1b'].arrays()
                    events_ee_boost = f['ee/boost'].arrays()

                    events_emu_res2b = f['emu/res2b'].arrays()
                    events_emu_res1b = f['emu/res1b'].arrays()
                    events_emu_boost = f['emu/boost'].arrays()

                    events_mumu_res2b = f['mumu/res2b'].arrays()
                    events_mumu_res1b = f['mumu/res1b'].arrays()
                    events_mumu_boost = f['mumu/boost'].arrays()

                    #Quick fix to have data not scaled by lum
                    tmp_lumi_bank = self.lum
                    if key == 'data_obs': self.lum = 1.0
                    weights_ee_res2b_dict = self.res_event_weight(weights_ee_res2b_dict, events_ee_res2b, sumGenWeight)
                    var_ee_res2b = np.concatenate((var_ee_res2b, events_ee_res2b[variable]))

                    weights_ee_res1b_dict = self.res_event_weight(weights_ee_res1b_dict, events_ee_res1b, sumGenWeight)
                    var_ee_res1b = np.concatenate((var_ee_res1b, events_ee_res1b[variable]))

                    weights_ee_boost_dict = self.boost_event_weight(weights_ee_boost_dict, events_ee_boost, sumGenWeight)
                    var_ee_boost = np.concatenate((var_ee_boost, events_ee_boost[variable]))

                    weights_emu_res2b_dict = self.res_event_weight(weights_emu_res2b_dict, events_emu_res2b, sumGenWeight)
                    var_emu_res2b = np.concatenate((var_emu_res2b, events_emu_res2b[variable]))

                    weights_emu_res1b_dict = self.res_event_weight(weights_emu_res1b_dict, events_emu_res1b, sumGenWeight)
                    var_emu_res1b = np.concatenate((var_emu_res1b, events_emu_res1b[variable]))

                    weights_emu_boost_dict = self.res_event_weight(weights_emu_boost_dict, events_emu_boost, sumGenWeight)
                    var_emu_boost = np.concatenate((var_emu_boost, events_emu_boost[variable]))

                    weights_mumu_res2b_dict = self.res_event_weight(weights_mumu_res2b_dict, events_mumu_res2b, sumGenWeight)
                    var_mumu_res2b = np.concatenate((var_mumu_res2b, events_mumu_res2b[variable]))

                    weights_mumu_res1b_dict = self.res_event_weight(weights_mumu_res1b_dict, events_mumu_res1b, sumGenWeight)
                    var_mumu_res1b = np.concatenate((var_mumu_res1b, events_mumu_res1b[variable]))

                    weights_mumu_boost_dict = self.res_event_weight(weights_mumu_boost_dict, events_mumu_boost, sumGenWeight)
                    var_mumu_boost = np.concatenate((var_mumu_boost, events_mumu_boost[variable]))
                    if key == 'data_obs': self.lum = tmp_lumi_bank

                nBins = 20
                lims=(0.0,500.0)
                uncert_list = ['nom', 'puUp', 'puDown', 'eleIDUp', 'eleIDDown', 'muIDUp', 'muIDDown']
                for uncert in uncert_list:
                    if (key == "data_obs") and (uncert != 'nom'): continue
                    dictname = key+"_"+uncert
                    if uncert == 'nom': dictname = key
                    outfile['ee/res2b/'+dictname] = np.histogram(np.array(var_ee_res2b), weights=np.array(weights_ee_res2b_dict[uncert]), bins=nBins, range=lims)
                    outfile['ee/res1b/'+dictname] = np.histogram(np.array(var_ee_res1b), weights=np.array(weights_ee_res1b_dict[uncert]), bins=nBins, range=lims)
                    outfile['ee/boost/'+dictname] = np.histogram(np.array(var_ee_boost), weights=np.array(weights_ee_boost_dict[uncert]), bins=nBins, range=lims)

                    outfile['emu/res2b/'+dictname] = np.histogram(np.array(var_emu_res2b), weights=np.array(weights_emu_res2b_dict[uncert]), bins=nBins, range=lims)
                    outfile['emu/res1b/'+dictname] = np.histogram(np.array(var_emu_res1b), weights=np.array(weights_emu_res1b_dict[uncert]), bins=nBins, range=lims)
                    outfile['emu/boost/'+dictname] = np.histogram(np.array(var_emu_boost), weights=np.array(weights_emu_boost_dict[uncert]), bins=nBins, range=lims)

                    outfile['mumu/res2b/'+dictname] = np.histogram(np.array(var_mumu_res2b), weights=np.array(weights_mumu_res2b_dict[uncert]), bins=nBins, range=lims)
                    outfile['mumu/res1b/'+dictname] = np.histogram(np.array(var_mumu_res1b), weights=np.array(weights_mumu_res1b_dict[uncert]), bins=nBins, range=lims)
                    outfile['mumu/boost/'+dictname] = np.histogram(np.array(var_mumu_boost), weights=np.array(weights_mumu_boost_dict[uncert]), bins=nBins, range=lims)


    def plot_data_vs_background(self, indir):
        def make_plot(category, name):
            data = cateogry.Get('data_obs')
            htt = cateogry.Get('TT')
            hdy = cateogry.Get('DY')
            hst = cateogry.Get('ST')
            ho = cateogry.Get('Other')
            hbkg = cateogry.Clone()
            hbkg.Add(hdy)
            hbkg.Add(hst)
            hbkg.Add(ho)
            hbkg.SetLineColor(ROOT.kRed)
            c1 = ROOT.TCanvas("c1", "c1", 800, 600)
            data.Draw()
            hbkg.Draw("same")
            c1.SaveAs(indir+name+".pdf")
        fname_list = [indir+fname for fname in os.listdir(indir)]
        for fname in fname_list:
            print("Looking at file ", fname)
            f = ROOT.TFile(fname)
            ee = f.Get('ee')
            ee_res2b = ee.Get('res2b')
            make_plot(ee_res2b, 'ee_res2b')

            ee_res1b = ee.Get('res1b')
            make_plot(ee_res1b, 'ee_res1b')

            ee_boost = ee.Get('boost')
            make_plot(ee_boost, 'ee_boost')

            emu = f.Get('emu')
            emu_res2b = emu.Get('res2b')
            make_plot(emu_res2b, 'emu_res2b')

            emu_res1b = emu.Get('res1b')
            make_plot(emu_res1b, 'emu_res1b')

            emu_boost = emu.Get('boost')
            make_plot(emu_boost, 'emu_boost')

            mumu = f.Get('mumu')
            mumu_res2b = mumu.Get('res2b')
            make_plot(mumu_res2b, 'mumu_res2b')

            mumu_res1b = ee.Get('res1b')
            make_plot(mumu_res1b, 'mumu_res1b')

            mumu_boost = mumu.Get('boost')
            make_plot(mumu_boost, 'mumu_boost')

            f.Close()

    def data_mc_plotter(self, filedir, out_dir):
        os.makedirs(out_dir, exist_ok = True)
        for fname in os.listdir(filedir):
            f = ROOT.TFile(filedir+fname)
            ee = f.Get("ee")
            emu = f.Get("emu")
            mumu = f.Get("mumu")
            for channel in ["ee", "emu", "mumu"]:
                for cat in ["res2b", "res1b", "boost"]:
                    step1 = f.Get(channel)
                    step2 = step1.Get(cat)
                    data = step2.Get("data_obs")
                    TT = step2.Get("TT")
                    DY = step2.Get("DY")
                    ST = step2.Get("ST")
                    Other = step2.Get("Other")
                    bkg = TT.Clone()
                    bkg.Add(DY)
                    bkg.Add(ST)
                    bkg.Add(Other)
                    bkg.SetLineColor(ROOT.kRed)
                    c1 = ROOT.TCanvas("c1", "c1", 800, 600)
                    data.SetTitle(fname[:-5]+" "+channel+" "+cat)
                    data.Draw()
                    bkg.Draw("same")
                    c1.SaveAs(out_dir+fname[:-5]+"_"+channel+"_"+cat+"_dataMC.pdf")


#shapemaker = CombineShapeMaker()
#storage_dir = "/eos/user/d/daebi/2022_data_13Apr24/"
#out_dir = "small_files_29May24_split/"
#shapemaker.prepare_all_datasets(storage_dir, out_dir)
#filedict = shapemaker.make_process_dict(out_dir)
#shapemaker.create_shape_file(filedict, out_dir, "shape_files_29May24_split/")

#out_dir = "combine_small_files_datainc/"
#shapemaker.prepare_all_datasets(storage_dir, out_dir)
#filedict = shapemaker.make_process_dict(out_dir)
#shapemaker.create_shape_file_blind(filedict, out_dir, "shape_files_datainc/", "hbb_mass")
#shapemaker.plot_data_vs_background("shape_files_datainc/")
#shapemaker.data_mc_plotter("shape_files_datainc/", "dataMC_plots/")
