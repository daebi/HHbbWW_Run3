import ROOT

sig250 = ROOT.TFile("../dnn/input_files/4apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-250_PreEE.root")
sig260 = ROOT.TFile("../dnn/input_files/4apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-260_PreEE.root")
sig270 = ROOT.TFile("../dnn/input_files/4apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-270_PreEE.root")
sig280 = ROOT.TFile("../dnn/input_files/4apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-280_PreEE.root")
sig300 = ROOT.TFile("../dnn/input_files/4apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-300_PreEE.root")
sig350 = ROOT.TFile("../dnn/input_files/4apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-350_PreEE.root")
sig450 = ROOT.TFile("../dnn/input_files/4apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-450_PreEE.root")
sig550 = ROOT.TFile("../dnn/input_files/4apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-550_PreEE.root")
sig600 = ROOT.TFile("../dnn/input_files/4apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-600_PreEE.root")
sig650 = ROOT.TFile("../dnn/input_files/4apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-650_PreEE.root")
sig700 = ROOT.TFile("../dnn/input_files/4apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-700_PreEE.root")
sig800 = ROOT.TFile("../dnn/input_files/4apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-800_PreEE.root")

tt = ROOT.TFile("../dnn/input_files/4apr/preEE/TTto2L2Nu_PreEE.root")


sig250_tree = sig250.Get("Double_HME_MT2_Tree")
sig260_tree = sig260.Get("Double_HME_MT2_Tree")
sig270_tree = sig270.Get("Double_HME_MT2_Tree")
sig280_tree = sig280.Get("Double_HME_MT2_Tree")
sig300_tree = sig300.Get("Double_HME_MT2_Tree")
sig350_tree = sig350.Get("Double_HME_MT2_Tree")
sig450_tree = sig450.Get("Double_HME_MT2_Tree")
sig550_tree = sig550.Get("Double_HME_MT2_Tree")
sig600_tree = sig600.Get("Double_HME_MT2_Tree")
sig650_tree = sig650.Get("Double_HME_MT2_Tree")
sig700_tree = sig700.Get("Double_HME_MT2_Tree")
sig800_tree = sig800.Get("Double_HME_MT2_Tree")



tt_tree = tt.Get("Double_HME_MT2_Tree")


h250 = ROOT.TH1D("h250", "h250", 60, 0, 600)
h260 = ROOT.TH1D("h260", "h260", 60, 0, 600)
h270 = ROOT.TH1D("h270", "h270", 60, 0, 600)
h280 = ROOT.TH1D("h280", "h280", 60, 0, 600)
h300 = ROOT.TH1D("h300", "h300", 60, 0, 600)
h350 = ROOT.TH1D("h350", "h350", 60, 0, 600)
h450 = ROOT.TH1D("h450", "h450", 60, 0, 600)
h550 = ROOT.TH1D("h550", "h550", 60, 0, 600)
h600 = ROOT.TH1D("h600", "h600", 60, 0, 600)
h650 = ROOT.TH1D("h650", "h650", 60, 0, 600)
h700 = ROOT.TH1D("h700", "h700", 60, 0, 600)
h800 = ROOT.TH1D("h800", "h800", 60, 0, 600)

htt = ROOT.TH1D("htt", "htt", 60, 0, 600)


h250.SetLineColor(ROOT.kBlue)
h300.SetLineColor(ROOT.kRed)
h350.SetLineColor(ROOT.kGreen+2)
h450.SetLineColor(ROOT.kMagenta)
h550.SetLineColor(ROOT.kOrange)
h600.SetLineColor(ROOT.kCyan)
h700.SetLineColor(ROOT.kYellow-3)
h800.SetLineColor(ROOT.kPink+4)

htt.SetLineColor(ROOT.kBlack)

h250.SetLineWidth(2)
h300.SetLineWidth(2)
h350.SetLineWidth(2)
h450.SetLineWidth(2)
h550.SetLineWidth(2)
h600.SetLineWidth(2)
h700.SetLineWidth(2)
h800.SetLineWidth(2)

htt.SetLineWidth(2)

h250.SetStats(0)

c = ROOT.TCanvas("c", "c", 800, 600)

sig250_tree.Draw("mt2_ll_lester >> h250")
sig260_tree.Draw("mt2_ll_lester >> h260")
sig270_tree.Draw("mt2_ll_lester >> h270")
sig280_tree.Draw("mt2_ll_lester >> h280")
sig300_tree.Draw("mt2_ll_lester >> h300")
sig350_tree.Draw("mt2_ll_lester >> h350")
sig450_tree.Draw("mt2_ll_lester >> h450")
sig550_tree.Draw("mt2_ll_lester >> h550")
sig600_tree.Draw("mt2_ll_lester >> h600")
sig650_tree.Draw("mt2_ll_lester >> h650")
sig700_tree.Draw("mt2_ll_lester >> h700")
sig800_tree.Draw("mt2_ll_lester >> h800")

tt_tree.Draw("mt2_ll_lester >> htt")

h250.Scale(1.0/h250.GetEntries())
h300.Scale(1.0/h300.GetEntries())
h350.Scale(1.0/h350.GetEntries())
h450.Scale(1.0/h450.GetEntries())
h550.Scale(1.0/h550.GetEntries())
h600.Scale(1.0/h600.GetEntries())
h700.Scale(1.0/h700.GetEntries())
h800.Scale(1.0/h800.GetEntries())

htt.Scale(1.0/htt.GetEntries())

h250.SetTitle("mt2_ll")

h250.Draw("hist")
h300.Draw("hist same")
h350.Draw("hist same")
h450.Draw("hist same")
h550.Draw("hist same")
h600.Draw("hist same")
h700.Draw("hist same")
h800.Draw("hist same")

htt.Draw("hist same")

leg = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
leg.AddEntry(h250, "Radion m250")
leg.AddEntry(h300, "Radion m300")
leg.AddEntry(h350, "Radion m350")
leg.AddEntry(h450, "Radion m450")
leg.AddEntry(h550, "Radion m550")
leg.AddEntry(h600, "Radion m600")
leg.AddEntry(h700, "Radion m700")
leg.AddEntry(h800, "Radion m800")

leg.AddEntry(htt, "TT")

leg.Draw()

c.SaveAs("mt2_ll_dist.pdf")




sig250_tree.Draw("mt2_bb_lester >> h250")
sig260_tree.Draw("mt2_bb_lester >> h260")
sig270_tree.Draw("mt2_bb_lester >> h270")
sig280_tree.Draw("mt2_bb_lester >> h280")
sig300_tree.Draw("mt2_bb_lester >> h300")
sig350_tree.Draw("mt2_bb_lester >> h350")
sig450_tree.Draw("mt2_bb_lester >> h450")
sig550_tree.Draw("mt2_bb_lester >> h550")
sig600_tree.Draw("mt2_bb_lester >> h600")
sig650_tree.Draw("mt2_bb_lester >> h650")
sig700_tree.Draw("mt2_bb_lester >> h700")
sig800_tree.Draw("mt2_bb_lester >> h800")

tt_tree.Draw("mt2_bb_lester >> htt")

h250.Scale(1.0/h250.GetEntries())
h300.Scale(1.0/h300.GetEntries())
h350.Scale(1.0/h350.GetEntries())
h450.Scale(1.0/h450.GetEntries())
h550.Scale(1.0/h550.GetEntries())
h600.Scale(1.0/h600.GetEntries())
h700.Scale(1.0/h700.GetEntries())
h800.Scale(1.0/h800.GetEntries())

htt.Scale(1.0/htt.GetEntries())

h250.SetTitle("mt2_bb")

h250.Draw("hist")
h300.Draw("hist same")
h350.Draw("hist same")
h450.Draw("hist same")
h550.Draw("hist same")
h600.Draw("hist same")
h700.Draw("hist same")
h800.Draw("hist same")

htt.Draw("hist same")

leg.Draw()

c.SaveAs("mt2_bb_dist.pdf")


sig250_tree.Draw("mt2_blbl_lester >> h250")
sig260_tree.Draw("mt2_blbl_lester >> h260")
sig270_tree.Draw("mt2_blbl_lester >> h270")
sig280_tree.Draw("mt2_blbl_lester >> h280")
sig300_tree.Draw("mt2_blbl_lester >> h300")
sig350_tree.Draw("mt2_blbl_lester >> h350")
sig450_tree.Draw("mt2_blbl_lester >> h450")
sig550_tree.Draw("mt2_blbl_lester >> h550")
sig600_tree.Draw("mt2_blbl_lester >> h600")
sig650_tree.Draw("mt2_blbl_lester >> h650")
sig700_tree.Draw("mt2_blbl_lester >> h700")
sig800_tree.Draw("mt2_blbl_lester >> h800")

tt_tree.Draw("mt2_blbl_lester >> htt")

h250.Scale(1.0/h250.GetEntries())
h300.Scale(1.0/h300.GetEntries())
h350.Scale(1.0/h350.GetEntries())
h450.Scale(1.0/h450.GetEntries())
h550.Scale(1.0/h550.GetEntries())
h600.Scale(1.0/h600.GetEntries())
h700.Scale(1.0/h700.GetEntries())
h800.Scale(1.0/h800.GetEntries())

htt.Scale(1.0/htt.GetEntries())

h250.SetTitle("mt2_blbl")

h250.Draw("hist")
h300.Draw("hist same")
h350.Draw("hist same")
h450.Draw("hist same")
h550.Draw("hist same")
h600.Draw("hist same")
h700.Draw("hist same")
h800.Draw("hist same")

htt.Draw("hist same")

leg.Draw()

c.SaveAs("mt2_blbl_dist.pdf")
