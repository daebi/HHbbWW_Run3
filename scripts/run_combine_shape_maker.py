from combine_shape_maker import CombineShapeMaker

shapemaker = CombineShapeMaker()
storage_dir = "/eos/user/d/daebi/2022_data_13Apr24/"
out_dir = "small_files_31May24_split/"
shapemaker.prepare_all_datasets(storage_dir, out_dir)
filedict = shapemaker.make_process_dict(out_dir)
shapemaker.create_shape_file(filedict, out_dir, "shape_files_31May24_split/")

#out_dir = "combine_small_files_datainc/"
#shapemaker.prepare_all_datasets(storage_dir, out_dir)
#filedict = shapemaker.make_process_dict(out_dir)
#shapemaker.create_shape_file_blind(filedict, out_dir, "shape_files_datainc/", "hbb_mass")
#shapemaker.plot_data_vs_background("shape_files_datainc/")
#shapemaker.data_mc_plotter("shape_files_datainc/", "dataMC_plots/")
