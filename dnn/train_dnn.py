from dnn import DNN_Model
import os

quiet = 1
debug = 0


for i in range(1):
    outname = "signal_tt_dy_st_other"
    dnn = DNN_Model(quiet, debug, outname)

    dnn.add_class(0, "Signal")
    dnn.add_class(1, "TT")
    dnn.add_class(2, "DY")
    dnn.add_class(3, "ST")
    #dnn.add_class(4, "Other")

    dnn.set_masspoints(["250", "260", "270", "280", "300", "350", "450", "550", "600", "650", "700", "800"])

    dnn.add_to_class_dict(0, "input_files/24apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-250_PreEE.root", param=250)
    dnn.add_to_class_dict(0, "input_files/24apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-260_PreEE.root", param=260)
    dnn.add_to_class_dict(0, "input_files/24apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-270_PreEE.root", param=270)
    dnn.add_to_class_dict(0, "input_files/24apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-280_PreEE.root", param=280)
    dnn.add_to_class_dict(0, "input_files/24apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-300_PreEE.root", param=300)
    dnn.add_to_class_dict(0, "input_files/24apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-350_PreEE.root", param=350)
    dnn.add_to_class_dict(0, "input_files/24apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-450_PreEE.root", param=450)
    dnn.add_to_class_dict(0, "input_files/24apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-550_PreEE.root", param=550)
    dnn.add_to_class_dict(0, "input_files/24apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-600_PreEE.root", param=600)
    dnn.add_to_class_dict(0, "input_files/24apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-650_PreEE.root", param=650)
    dnn.add_to_class_dict(0, "input_files/24apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-700_PreEE.root", param=700)
    dnn.add_to_class_dict(0, "input_files/24apr/preEE/GluGlutoRadiontoHHto2B2Vto2B2L2Nu_M-800_PreEE.root", param=800)

    dnn.add_to_class_dict(1, "input_files/24apr/preEE/TTto2L2Nu_PreEE.root", xs=98.036113)

    dnn.add_to_class_dict(2, "input_files/24apr/preEE/DYJetsToLL_M-50_PreEE.root", xs=5659.0)
    dnn.add_to_class_dict(2, "input_files/24apr/preEE/DYto2L-2Jets_MLL-10to50_PreEE.root", xs=19982.5)

    dnn.add_to_class_dict(3, "input_files/24apr/preEE/TbarWplusto2L2Nu_PreEE.root", xs=4.6651009)
    dnn.add_to_class_dict(3, "input_files/24apr/preEE/TWminusto2L2Nu_PreEE.root", xs=4.6651009)

    #dnn.add_to_class_dict(4, "input_files/24apr/preEE/WW_PreEE.root", xs=80.18)
    #dnn.add_to_class_dict(4, "input_files/24apr/preEE/WZ_PreEE.root", xs=29.1)
    #dnn.add_to_class_dict(4, "input_files/24apr/preEE/ZZ_PreEE.root", xs=12.75)

    dnn.finalize_class_dict()

    dnn.train_model()
