import tensorflow as tf
import os
import uproot
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelBinarizer
import sklearn.metrics
from keras.wrappers.scikit_learn import KerasClassifier, KerasRegressor
import datetime
import random


class DNN_Model():
    def __init__(self, quiet=0, debug=0, outname="model"):
        self.quiet = quiet
        self.debug = debug
        self.do_feat_imp = False
        self.curr_time = datetime.datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
        self.model_folder = "DNN_Models/DNN_Model_"+self.curr_time+"/"
        self.model_name = outname

        self.input_labels = [
            "Lep0 px", "Lep0 py", "Lep0 pz", "Lep0 E", "Lep0 pdgId", "Lep0 charge",
            "Lep1 px", "Lep1 py", "Lep1 pz", "Lep1 E", "Lep1 pdgId", "Lep1 charge",
            "Ak4 Jet0 px", "Ak4 Jet0 py", "Ak4 Jet0 pz", "Ak4 Jet0 E", "Ak4 Jet0 btagDeepFlavB",
            "Ak4 Jet1 px", "Ak4 Jet1 py", "Ak4 Jet1 pz", "Ak4 Jet1 E", "Ak4 Jet1 btagDeepFlavB",
            "Ak4 Jet2 px", "Ak4 Jet2 py", "Ak4 Jet2 pz", "Ak4 Jet2 E", "Ak4 Jet2 btagDeepFlavB",
            "Ak4 Jet3 px", "Ak4 Jet3 py", "Ak4 Jet3 pz", "Ak4 Jet3 E", "Ak4 Jet3 btagDeepFlavB",
            "AK8 Jet0 px", "Ak8 Jet0 py", "Ak8 Jet0 pz", "Ak8 Jet0 E",
            "Ak8 Jet0 Tau1", "Ak8 Jet0 Tau2", "Ak8 Jet0 Tau3", "Ak8 Jet0 Tau4",
            "MET px", "MET py", "MET pz", "MET E",
            "HT",
            "ll px", "ll py", "ll pz", "ll E", "ll mass",
            "hWW px", "hWW py", "hWW pz", "hWW E", "hWW mass",
            "hbb px", "hbb py", "hbb pz", "hbb E", "hbb mass",
            "hh px", "hh py", "hh pz", "hh E", "hh mass",
            "n Fakeable Muons",
            "n Fakeable Electrons",
            "n Cleaned Ak4 Jets",
            "n Medium B Ak4 Jets",

            "dR_dilep",
            "dR_dibjet",
            "dR_dilep_dijet",
            "dR_dilep_dibjet",
            "dPhi_MET_dilep",
            "dPhi_MET_dibjet",
            "min_dR_lep0_cleanAk4",
            "min_dR_lep1_cleanAk4",
            "min_dR_ak4_jet0_leadleps",
            "min_dR_ak4_jet1_leadleps",
            "min_dR_cleaned_ak4_jets",
            "min_absdPhi_cleaned_ak4_jets",

            "ll_met_mt",
            "bb_met_mt",
            "b_jet_costheta",

            "mt2_ll",
            "mt2_bb",
            "mt2_blbl",

            "Double_Res_1b",
            "Double_Res_2b",
            "Double_HbbFat",


            "HME",

            "param"
        ]

        self.class_dict = {}

        self.class_dict_finalized = False

        self.class_labels = []

        self.full_array = []
        self.full_label = []
        self.full_weight = []

        self.model = []

        self.parametric_masspoints = []

    def set_masspoints(self, masslist):
        self.parametric_masspoints = masslist

    def add_class(self, classnumber, classname):
        self.class_dict[str(classnumber)] = {}
        self.class_dict[str(classnumber)]['classname'] = classname
        self.class_dict[str(classnumber)]['filelist'] = []
        self.class_dict[str(classnumber)]['param'] = []
        self.class_dict[str(classnumber)]['eventslist'] = []
        self.class_dict[str(classnumber)]['classweight'] = 0.0
        self.class_dict[str(classnumber)]['arrays'] = []
        self.class_dict[str(classnumber)]['labels'] = []
        self.class_dict[str(classnumber)]['weights'] = []
        self.class_dict[str(classnumber)]['xs'] = []
        #self.class_dict[str(classnumber)]['sumGenWeight'] = []
        print("Added ", classname, " to dict with number ", classnumber)

    #def add_to_class_dict(self, classification, filename, param=[250,1000], xs=1.0):
    def add_to_class_dict(self, classification, filename, param=0, xs=1.0):
        classification = str(classification)
        self.class_dict[classification]['filelist'].append(filename)
        print("Current list ", self.class_dict[classification]['param'])
        print("Appending ", param)
        self.class_dict[classification]['param'].append(param)
        self.class_dict[classification]['xs'].append(xs)


    def finalize_class_dict(self):
        array_list = []
        label_list = []
        weight_list = []

        nEventsToUse = 1e10
        smallest_file = ""
        #Loop over all files we are looking at and normalize the number of events to use
        for classification in self.class_dict.keys():
            for filename in self.class_dict[classification]['filelist']:
                f = uproot.open(filename)
                t = f['Double_HME_MT2_Tree']
                events = t.arrays()
                nEventsThisFile = len(events)
                if nEventsThisFile < nEventsToUse:
                    nEventsToUse = nEventsThisFile
                    smallest_file = filename
            self.class_labels.append(self.class_dict[classification]['classname'])
        print("Smallest number of events is ", nEventsToUse)
        print("Limiting file is ", smallest_file)
        for classification in self.class_dict.keys():
            nEvtsClass = 0.0
            totClassWeight = 0.0
            for i, filename in enumerate(self.class_dict[classification]['filelist']):
                f = uproot.open(filename)
                t = f['Double_HME_MT2_Tree']
                #nEvts = np.sum(f['nEvents'].arrays())
                #print("Found nEvts in file ", nEvts)
                events = self.prepare_events(t)#[:10000] #Experimental! Having a problem with overfitting
                #events = events[:min(len(events), 100000)]
                array = (self.create_nparray(events, self.class_dict[classification]['param'][i])).transpose()
                nEvtsClass += len(events)
                totClassWeight += len(events)*self.class_dict[classification]['xs'][i]
                self.class_dict[classification]['eventslist'].append(events)
                self.class_dict[classification]['arrays'].append(array)
                self.class_dict[classification]['labels'].append(np.full(len(array), int(classification)))
                #self.class_dict[classification]['weights'].append(np.full(len(array), self.class_dict[classification]['xs'][i]))
                self.class_dict[classification]['weights'].append(np.array(events.genWeight * self.class_dict[classification]['xs'][i]))
                #self.class_dict[classification]['sumGenWeight'].append(np.sum((f['nEvents'].arrays()['sumGenWeight'])))
                array_list.append(array)
                label_list.append(np.full(len(array), int(classification)))

                if self.class_dict[classification]['classname'] == "Signal": self.class_dict[classification]['weights'][i] = self.class_dict[classification]['weights'][i]/len(events)
                else: self.class_dict[classification]['weights'][i] = self.class_dict[classification]['weights'][i] / np.sum((f['nEvents'].arrays()['sumGenWeight']))
                #weight_list.append(self.class_dict[classification]['weights'][i])

                print("For file ", filename, " we are using weight ", self.class_dict[classification]['weights'][i][0], " on ", len(events), " events")
                print("So total weight in file is ", self.class_dict[classification]['weights'][i][0]*len(events))
                self.class_dict[classification]['classweight'] += self.class_dict[classification]['weights'][i][0]*len(events)

            #if nEvtsClass != 0: self.class_dict[classification]['classweight'] *= 1.0/nEvtsClass
            print("Class ", classification, " has total weight ", self.class_dict[classification]['classweight'])
            tmp_total = self.class_dict[classification]['classweight']
            self.class_dict[classification]['classweight'] = 0.0
            for i in range(len(self.class_dict[classification]['filelist'])):
                self.class_dict[classification]['weights'][i] = self.class_dict[classification]['weights'][i] * 1.0/tmp_total
                weight_list.append(self.class_dict[classification]['weights'][i])
                self.class_dict[classification]['classweight'] += self.class_dict[classification]['weights'][i][0]*len(self.class_dict[classification]['weights'][i])
            print("Rescaled, it has weight  ", self.class_dict[classification]['classweight'])


        self.full_array = np.concatenate(array_list)
        self.full_label = np.concatenate(label_list)
        self.full_weight = np.concatenate(weight_list)

        self.class_dict_finalized = True



    """
    def create_nparray(self, events, param=0):
        #param input is the parametric DNN hh mass
        #for signal, we give true mass, for background we give random
        param_vals = []
        if param:
            param_vals = np.full(len(events), param).astype(float)
        else:
            param_vals = np.random.choice(self.parametric_masspoints, size=len(events)).astype(float)
        print("Chosen param vals is ", param_vals)
        array = np.array([
            #events.lep0_px,
            #events.lep0_py,
            #events.lep0_pz,
            #events.lep0_E,
            #events.lep0_pdgId,
            #events.lep0_charge,

            #events.lep1_px,
            #events.lep1_py,
            #events.lep1_pz,
            #events.lep1_E,
            #events.lep1_pdgId,
            #events.lep1_charge,

            #events.ak4_jet0_px,
            #events.ak4_jet0_py,
            #events.ak4_jet0_pz,
            #events.ak4_jet0_E,
            #events.ak4_jet0_btagDeepFlavB,

            #events.ak4_jet1_px,
            #events.ak4_jet1_py,
            #events.ak4_jet1_pz,
            #events.ak4_jet1_E,
            #events.ak4_jet1_btagDeepFlavB,

            #events.ak4_jet2_px,
            #events.ak4_jet2_py,
            #events.ak4_jet2_pz,
            #events.ak4_jet2_E,
            #events.ak4_jet2_btagDeepFlavB,

            #events.ak4_jet3_px,
            #events.ak4_jet3_py,
            #events.ak4_jet3_pz,
            #events.ak4_jet3_E,
            #events.ak4_jet3_btagDeepFlavB,

            #events.ak8_jet0_px,
            #events.ak8_jet0_py,
            #events.ak8_jet0_pz,
            #events.ak8_jet0_E,
            #events.ak8_jet0_tau1,
            #events.ak8_jet0_tau2,
            #events.ak8_jet0_tau3,
            #events.ak8_jet0_tau4,

            #events.met_px,
            #events.met_py,
            #events.met_pz,
            #events.met_E,

            #events.HT,

            #events.ll_px,
            #events.ll_py,
            #events.ll_pz,
            #events.ll_E,
            #events.ll_mass,

            #events.hWW_px,
            #events.hWW_py,
            #events.hWW_pz,
            #events.hWW_E,
            #events.hWW_mass,

            #events.hbb_px,
            #events.hbb_py,
            #events.hbb_pz,
            #events.hbb_E,
            #events.hbb_mass,

            #events.hh_px,
            #events.hh_py,
            #events.hh_pz,
            #events.hh_E,
            #events.hh_mass,

            #events.n_fakeable_muons,
            #events.n_fakeable_electrons,
            #events.n_cleaned_ak4_jets,
            #events.n_medium_btag_ak4_jets,

            #events.dR_dilep,
            #events.dR_dibjet,
            #events.dR_dilep_dijet,
            #events.dR_dilep_dibjet,
            #events.dPhi_MET_dilep,
            #events.dPhi_MET_dibjet,
            #events.min_dR_lep0_cleanAk4,
            #events.min_dR_lep1_cleanAk4,
            #events.min_dR_ak4_jet0_leadleps,
            #events.min_dR_ak4_jet1_leadleps,
            #events.min_dR_cleaned_ak4_jets,
            #events.min_absdPhi_cleaned_ak4_jets,

            #events.ll_met_mt,
            #events.bb_met_mt,
            #events.b_jet_costheta,

            #events.mt2_ll_lester,
            #events.mt2_bb_lester,
            #events.mt2_blbl_lester,

            #events.Double_Res_1b,
            #events.Double_Res_2b,
            #events.Double_HbbFat,

            events.HME,

            param_vals

        ])
        return array

    """

    def create_nparray(self, events, param=0):
        #param input is the parametric DNN hh mass
        #for signal, we give true mass, for background we give random
        param_vals = []
        if param:
            param_vals = np.full(len(events), param).astype(float)
        else:
            param_vals = np.random.choice(self.parametric_masspoints, size=len(events)).astype(float)
        if self.debug: print("Chosen param vals is ", param_vals)
        array = np.array([
            events.lep0_pt,
            events.lep0_E,
            events.lep0_pdgId,
            events.lep0_charge,

            events.lep1_pt,
            events.lep1_E,
            events.lep1_pdgId,
            events.lep1_charge,

            events.met_px,
            events.met_py,
            events.met_E,

            events.HT,

            events.Double_HbbFat,

            events.dR_dilep,
            events.dR_dibjet,
            events.dR_dilep_dijet,
            events.dR_dilep_dibjet,
            events.dPhi_MET_dilep,
            events.dPhi_MET_dibjet,
            events.min_dR_lep0_cleanAk4,
            events.min_dR_lep1_cleanAk4,
            events.min_dR_ak4_jet0_leadleps,
            events.min_dR_ak4_jet1_leadleps,
            events.min_dR_cleaned_ak4_jets,
            events.min_absdPhi_cleaned_ak4_jets,

            events.b_jet_costheta,

            events.mt2_blbl_lester,

            events.HME,

            #events.dnn_truth_value, #Super debug variable, process truth so this should give 100% accuracy

            param_vals

        ])
        return array

    def prepare_events(self, tree):
        events = tree.arrays()
        #mask = (events.Double_Res_2b == 1) & (events.Double_Signal == 1) & (events.HME > 0)
        mask = (events.Double_Signal == 1) & (events.HME > 0)
        events_filtered = events[mask]
        return events_filtered

    def train_model(self):
        if not self.class_dict_finalized:
            print("YOU DID NOT FINALIZE THE CLASS DICT!")
            return

        print("Starting DNN training process")
        print("Current model will be saved in "+self.model_folder)
        print("Saving this file ", __file__, " into folder for future reference")
        os.makedirs(self.model_folder, exist_ok = True)
        os.makedirs(self.model_folder+"/plots/", exist_ok = True)
        os.system("cp {file} {folder}".format(file = __file__, folder = self.model_folder))


        if not self.quiet:
            print("Input comments about this training")
            comments_file = self.model_folder+"comments.txt"
            with open(comments_file, 'w', encoding='utf-8') as my_file:
                my_file.write(input('Comments: '))


        tf.keras.backend.clear_session()

        #How many inputs?
        input_len = len(self.full_array[0])


        #Prepare train and test samples, as well as random states
        events = self.full_array
        labels = self.full_label
        sampleweights = self.full_weight

        events_train, events_test, labels_train, labels_test, sampleweights_train, sampleweights_test  = train_test_split(events, labels, sampleweights, test_size=0.33)


        #labels_train = tf.keras.utils.to_categorical(labels_train, num_classes=4)
        #labels_test = tf.keras.utils.to_categorical(labels_test, num_classes=4)
        #Sparse categorical is better for mutually exclusive, and we do not need onehots for that

        # This function keeps the initial learning rate for the first ten epochs
        # and decreases it exponentially after that.
        def scheduler(epoch, lr):
            if epoch < 10:
                return lr
            else:
                return lr * tf.math.exp(-0.1)

        l1_value = 0.00001
        l2_value = 0.0001
        dropout_value = 0.1

        def base_model():
            model = tf.keras.Sequential()

            model.add(tf.keras.layers.Normalization(input_dim=input_len))
            #model.add(tf.keras.layers.Input(shape=input_len))

            model.add(tf.keras.layers.Dense(64, activation="relu"))#, kernel_regularizer=tf.keras.regularizers.L1L2(l1=l1_value, l2=l2_value)))

            model.add(tf.keras.layers.Dropout(dropout_value))

            model.add(tf.keras.layers.Dense(64, activation="relu"))#, kernel_regularizer=tf.keras.regularizers.L1L2(l1=l1_value, l2=l2_value)))

            model.add(tf.keras.layers.Dropout(dropout_value))

            model.add(tf.keras.layers.Dense(64, activation="relu"))#, kernel_regularizer=tf.keras.regularizers.L1L2(l1=l1_value, l2=l2_value)))

            model.add(tf.keras.layers.Dropout(dropout_value))

            model.add(tf.keras.layers.Dense(64, activation="relu"))#, kernel_regularizer=tf.keras.regularizers.L1L2(l1=l1_value, l2=l2_value)))

            model.add(tf.keras.layers.Dropout(dropout_value))

            model.add(tf.keras.layers.Dense(64, activation="relu"))#, kernel_regularizer=tf.keras.regularizers.L1L2(l1=l1_value, l2=l2_value)))

            model.add(tf.keras.layers.Dropout(dropout_value))

            model.add(tf.keras.layers.Dense(len(self.class_dict), activation="softmax"))


            model.compile(
                ### Adam optimizer, with initial lr = 0.001
                optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
                #loss=tf.keras.losses.CategoricalHinge(),
                loss=tf.keras.losses.SparseCategoricalCrossentropy(),
                weighted_metrics=[
                    #'accuracy',
                    tf.keras.metrics.SparseCategoricalCrossentropy(),
                    tf.keras.metrics.SparseCategoricalAccuracy(),
                    #tf.keras.metrics.CategoricalAccuracy(),
                    #tf.keras.metrics.CategoricalCrossentropy(),
                ]
            )
            return model

        def base_binary_model():
            model = tf.keras.Sequential()

            model.add(tf.keras.layers.Normalization(input_dim=input_len))
            #model.add(tf.keras.layers.Input(shape=input_len))

            model.add(tf.keras.layers.Dense(64, activation="relu"))#, kernel_regularizer=tf.keras.regularizers.L1L2(l1=l1_value, l2=l2_value)))

            model.add(tf.keras.layers.Dropout(dropout_value))

            model.add(tf.keras.layers.Dense(64, activation="relu"))#, kernel_regularizer=tf.keras.regularizers.L1L2(l1=l1_value, l2=l2_value)))

            model.add(tf.keras.layers.Dropout(dropout_value))

            model.add(tf.keras.layers.Dense(64, activation="relu"))#, kernel_regularizer=tf.keras.regularizers.L1L2(l1=l1_value, l2=l2_value)))

            model.add(tf.keras.layers.Dropout(dropout_value))

            model.add(tf.keras.layers.Dense(64, activation="relu"))#, kernel_regularizer=tf.keras.regularizers.L1L2(l1=l1_value, l2=l2_value)))

            model.add(tf.keras.layers.Dropout(dropout_value))

            model.add(tf.keras.layers.Dense(64, activation="relu"))#, kernel_regularizer=tf.keras.regularizers.L1L2(l1=l1_value, l2=l2_value)))

            model.add(tf.keras.layers.Dropout(dropout_value))

            model.add(tf.keras.layers.Dense(1, activation="sigmoid"))


            model.compile(
                ### Adam optimizer, with initial lr = 0.001
                optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
                #loss=tf.keras.losses.CategoricalHinge(),
                loss=tf.keras.losses.BinaryCrossentropy(),
                weighted_metrics=[
                    #'accuracy',
                    tf.keras.metrics.BinaryCrossentropy(),
                    tf.keras.metrics.BinaryAccuracy(),
                    #tf.keras.metrics.CategoricalAccuracy(),
                    #tf.keras.metrics.CategoricalCrossentropy(),
                ]
            )
            return model

        def fit_model(model, model_NamePath):
            history = model.fit(
                                events_train,
                                labels_train,
                                sample_weight = sampleweights_train,
                                validation_data=(
                                    events_test,
                                    labels_test,
                                    sampleweights_test
                                    ),
                                epochs=100,
                                batch_size=256,
                                # Callback: set of functions to be applied at given stages of the training procedure
                                callbacks=[
                                    #tf.keras.callbacks.ModelCheckpoint(model_NamePath, monitor='val_loss', verbose=False, save_best_only=True),
                                    tf.keras.callbacks.LearningRateScheduler(scheduler), # How this is different from 'conf.optimiz' ?
                                    #tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=5) # Stop once you stop improving the val_loss
                                    tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=5) # Stop once you stop improving the loss
                                    ]
                                )
            return history




        def make_performance_plots(model, test_events, test_labels, plot_prefix=""):
            predict_set = model.predict(test_events)

            cm = sklearn.metrics.confusion_matrix(np.argmax(test_labels, axis=1), np.argmax(predict_set, axis=1), normalize='true')
            disp = sklearn.metrics.ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=self.class_labels)
            disp.plot(cmap=plt.cm.Blues)
            plt.savefig(self.model_folder+"/plots/"+plot_prefix+"conf_matrix.pdf")
            plt.close()

            #Lets put the ROC curve for each class on a plot and calculate AUC
            g, c_ax = plt.subplots(1,1, figsize = (8,6))
            for (idx, c_label) in enumerate(self.class_labels):
                fpr, tpr, thresholds = sklearn.metrics.roc_curve(test_labels[:,idx].astype(int), predict_set[:,idx])
                c_ax.plot(fpr, tpr, label = "{label} (AUC: {auc})".format(label = c_label, auc = sklearn.metrics.auc(fpr, tpr)))

            c_ax.plot(fpr, fpr, "b-", label = "Random Guessing")

            c_ax.legend()
            c_ax.grid(True)
            c_ax.set_xlabel("False Positive Rate")
            c_ax.set_ylabel("True Positive Rate")
            g.savefig(self.model_folder+"/plots/"+plot_prefix+"roc_curves.pdf")
            g.clf()
            plt.close()
            #g.close()


        def make_history_plots(history, plot_prefix=""):
            #Lets look at training metrics, maybe just a plot of the history?
            plt.plot(history.history['sparse_categorical_accuracy'])
            plt.plot(history.history['val_sparse_categorical_accuracy'])
            plt.grid(True)
            plt.xlabel("Epoch")
            plt.ylabel("Categorical Accuracy")
            plt.ylim(0, 1.1)
            plt.legend(['train', 'test'], loc='upper left')
            plt.savefig(self.model_folder+"/plots/"+plot_prefix+"accuracy.pdf")
            plt.clf()
            plt.close()

            plt.plot(history.history['loss'])
            plt.plot(history.history['val_loss'])
            plt.grid(True)
            plt.yscale("log")
            plt.xlabel("Epoch")
            plt.ylabel("Loss")
            plt.legend(['train', 'test'], loc='upper left')
            plt.savefig(self.model_folder+"/plots/"+plot_prefix+"loss.pdf")
            plt.clf()
            plt.close()

        def validate_dnn_output(plot_prefix=""):
            plotbins = 100
            plotrange = (0.0, 1.0)
            #After discussion with Konstanin, parametric dnn application should put in mass values for the point we want
            #So we will have a DNN output for masspoint 300, 400, 500, etc
            #For now, lets create a parametric_masspoints list and apply with those masses
            #Theory is that these masses will work very well for signal at that mass, but still remove backgrounds

            #X points for the acceptance scan and significance scan
            x = np.linspace(0.0, 1.0, 101)
            all_significances = []
            all_acceptances = []
            for para_masspoint in self.parametric_masspoints:
                print("Looking at mass ", para_masspoint)
                predict_list = []
                weight_list = []
                fname_list = []
                xs_list = []
                os.makedirs(self.model_folder+"/plots/m"+para_masspoint+"/", exist_ok = True)
                for classification in self.class_dict.keys():
                    for count,fname in enumerate(self.class_dict[classification]['filelist']):
                        print("Classification is ", self.class_dict[classification]['classname'])
                        if self.class_dict[classification]['classname'] == 'Signal':
                            filemass = int((fname.split('-')[-1]).split('_')[0])
                            tmpmass = int(para_masspoint)
                            if abs(filemass-tmpmass) > 151:
                                continue
                        print("Going to predict ", fname)
                        uproot_file = uproot.open(fname)
                        key = 'Double_HME_MT2_Tree'
                        predict_list.append(self.predict(uproot_file[key], int(para_masspoint)))
                        #Weights should probably be handled in the self.predict function, for now it is Here
                        #But here is a bad choice due to the mask in prepare_events, this mask would have to be changed there AND here (HME mask)
                        weight_list.append((uproot_file[key].arrays()['genWeight']*self.class_dict[classification]['xs'][count]/np.sum((uproot_file['nEvents'].arrays()['sumGenWeight'])))[uproot_file[key].arrays()['HME'] > 0])
                        fname_list.append(fname)
                        xs_list.append(self.class_dict[classification]['xs'][count])

                for outnum in range(len(self.class_labels)):
                    for i in range(len(predict_list)):
                        plt.hist(predict_list[i][:,outnum], bins=plotbins, range=plotrange, density=True, histtype='step', label=fname_list[i].split('/')[-1], alpha=0.5)

                    plt.title('DNN Output: '+self.class_labels[outnum]+ ' m{m}'.format(m=para_masspoint))
                    plt.legend(loc='upper right', fontsize="4")
                    plt.yscale('log')
                    plt.savefig(self.model_folder+"/plots/m"+para_masspoint+"/"+plot_prefix+"dnn_values_"+self.class_labels[outnum]+".pdf")
                    plt.clf()
                    plt.close()


                    #Same hist, but now with realistic weights
                    for i in range(len(predict_list)):
                        plt.hist(predict_list[i][:,outnum], bins=plotbins, range=plotrange, density=True, histtype='step', label=fname_list[i].split('/')[-1], alpha=0.5, weights=weight_list[i])

                    plt.title('Weighted DNN Output: '+self.class_labels[outnum]+ ' m{m}'.format(m=para_masspoint))
                    plt.legend(loc='upper right', fontsize="4")
                    plt.yscale('log')
                    plt.savefig(self.model_folder+"/plots/m"+para_masspoint+"/"+plot_prefix+"weighted_dnn_values_"+self.class_labels[outnum]+".pdf")
                    plt.clf()
                    plt.close()


                    acceptance_list = []
                    for i in range(len(predict_list)):
                        tmp_list = []
                        for cut in x:
                            tmp_list.append(np.sum(predict_list[i][:,outnum] > cut)/len(predict_list[i]))
                        acceptance_list.append(tmp_list)
                        if "GluGluto" in fname_list[i]:
                            plt.plot(x, acceptance_list[i], marker="+", label=fname_list[i].split('/')[-1])
                        else:
                            plt.plot(x, acceptance_list[i], label=fname_list[i].split('/')[-1])
                    plt.title(self.class_labels[outnum]+' Acceptance, m{}'.format(para_masspoint))
                    plt.ylabel('Aceptance')
                    plt.xlabel('DNN Cut')
                    plt.legend(loc='upper right', fontsize="4")
                    #plt.yscale('log')
                    plt.savefig(self.model_folder+"/plots/m"+para_masspoint+"/"+plot_prefix+"dnn_acceptance_"+self.class_labels[outnum]+".pdf")
                    plt.clf()
                    plt.close()


                soverb = []
                soverrootb = []
                soverrootsplusb = []
                acceptance = []
                no_cut_bkg = 0
                no_cut_sig = 0
                for cut in x:
                    sig_tot = 0
                    bkg_tot = 0
                    for i in range(len(predict_list)):
                        fname = fname_list[i]
                        if 'GluGluto' in fname:
                            filemass = int((fname.split('-')[-1]).split('_')[0])
                            tmpmass = int(para_masspoint)
                            if filemass != tmpmass:
                                continue
                            sig_tot += (np.sum((predict_list[i][:,0] > cut)*weight_list[i]))
                        else:
                            bkg_tot += (np.sum((predict_list[i][:,0] > cut)*weight_list[i]))

                    if cut == 0:
                        no_cut_bkg = bkg_tot
                        no_cut_sig = sig_tot

                    print("At x cut ", cut, " the sig/bkg is ", sig_tot, "/", bkg_tot)

                    #if (bkg_tot <= 0) or (sig_tot <= 0.1*no_cut_sig):
                    if (bkg_tot <= 0):
                        bkg_tot = 10000 #Can't divide by 0, so basically set ratio to 0
                        #Also put a cut to require at least 10% of signal events to be kept (avoids 1% signal events having highest significance)

                    tmp_soverb = sig_tot/bkg_tot
                    soverb.append(tmp_soverb)
                    tmp_soverrootb = sig_tot/(bkg_tot**(0.5))
                    soverrootb.append(tmp_soverrootb)
                    tmp_soverrootsplusb = sig_tot/((sig_tot+bkg_tot)**(0.5))
                    soverrootsplusb.append(tmp_soverrootsplusb)
                    acceptance_tmp = sig_tot/no_cut_sig
                    acceptance.append(acceptance_tmp)

                #plt.plot(x, soverb, label='TotalSignal/TotalBackground')
                sig_max_index = np.argmax(soverrootb)
                sig_max = soverrootb[sig_max_index]
                cut_max = x[sig_max_index]
                plt.plot(x, soverrootb, label='TotalSignal/root(TotalBackground) MaxS={sig_max}, Cut={cut_max}'.format(sig_max = sig_max, cut_max = cut_max))
                #plt.plot(x, soverrootsplusb, label='TotalSignal/root(TotalSignal+TotalBackground)')
                plt.title('Significance, m{}'.format(para_masspoint))
                plt.ylabel('Significance')
                plt.xlabel('DNN Cut')
                plt.legend(loc='lower left', fontsize="4")
                #plt.yscale('log')
                plt.savefig(self.model_folder+"/plots/m"+para_masspoint+"/"+plot_prefix+"dnn_significance_Signal.pdf")
                plt.clf()
                plt.close()
                all_significances.append(soverrootb)
                all_acceptances.append(acceptance)

            for i, para_masspoint in enumerate(self.parametric_masspoints):
                sig_max_index = np.argmax(all_significances[i])
                sig_max = all_significances[i][sig_max_index]
                cut_max = x[sig_max_index]
                acceptance = all_acceptances[i][sig_max_index]
                plt.plot(x, all_significances[i], label='S/root(B) m{m} MaxS={sig_max}, Cut={cut_max}, Acc={acceptance}'.format(m = para_masspoint, sig_max = sig_max, cut_max = cut_max, acceptance = acceptance))
            plt.title('All Significance')
            plt.ylabel('Significance')
            plt.xlabel('DNN Cut')
            plt.legend(loc='lower left', fontsize="4")
            plt.yscale('log')
            plt.savefig(self.model_folder+"/plots/"+plot_prefix+"all_dnn_significance_Signal.pdf")
            plt.clf()
            plt.close()




        def do_feature_imp():
            print("Doing feature importance!")
            #Feature importance requires sklearn model
            import eli5
            from eli5.sklearn import PermutationImportance

            save_path = "./"+model_folder+"/SK_TT_ST_DY_signalM450"
            sk_model = KerasRegressor(build_fn=base_model)
            sk_history = fit_model(sk_model, save_path)
            #make_performance_plots(sk_model, events_test, labels_test, "sk_")
            #make_history_plots(sk_history, "sk_")

            perm = PermutationImportance(sk_model, n_iter=10, random_state=1).fit(events_train, labels_train)
            feat_import_dict = eli5.format_as_dict(eli5.explain_weights(perm, top=100))

            feat_import = feat_import_dict['feature_importances']['importances']
            print(eli5.format_as_text(eli5.explain_weights(perm, top=100, feature_names=input_labels)))
            return perm


        self.model = base_model()
        self.model.summary()
        save_path = "./"+self.model_folder+"/"+self.model_name
        history = fit_model(self.model, save_path)
        self.model.save(save_path)
        make_performance_plots(self.model, events_test, tf.keras.utils.to_categorical(labels_test, num_classes=len(self.class_labels)))
        make_history_plots(history)
        validate_dnn_output()


        #True binary test method, results looked identical to multiclass sig/bkg
        #self.model = base_binary_model()
        #self.model.summary()
        #save_path = "./"+self.model_folder+"/"+self.model_name
        #history = fit_model(self.model, save_path)
        #self.class_labels = ["Signal"]
        #validate_dnn_output()


        if self.do_feat_imp == True:
            do_feature_imp()



    def load_model(self, modelname):
        self.model = tf.keras.models.load_model(modelname)
        print("Loaded model ", modelname)
        print(self.model.summary())

    def predict(self, tree, param=0):
        #Never give the param to predict, should always be full range [300,700]
        if self.debug: print("Going to predict events on file ", filename, " with param ", param)
        events = self.prepare_events(tree)
        array = (self.create_nparray(events, param)).transpose()
        #norm_inputs = tf.keras.layers.Normalization(axis=-1)
        #norm_inputs.adapt(array)
        #array_norm = norm_inputs(array)
        pred = self.model.predict(array)
        return pred

    def save_outfile(self, inname, outname):
        infile = uproot.open(inname)
        outfile = uproot.recreate(outname)
        outfile["nEvents"] = infile['nEvents'].arrays()
        single_keys = []
        double_keys = []
        for key in infile.keys():
            if 'Single' in key: single_keys.append(key.split(';')[0])
            if 'Double' in key: double_keys.append(key.split(';')[0])

        for key in single_keys:
            syst = (key.split("Tree")[0]).split("Single_HME_MT2")[1]
            outfile['Single_HME_MT2_DNN_'+syst+'Tree'] = infile[key].arrays()

        for key in double_keys:
            print("At key ", key)
            double_events = infile[key].arrays()
            double_events = double_events[(double_events.Double_Signal == 1) & (double_events.HME > 0)]
            if len(double_events) == 0:
                print("No double events for DNN!")
                return
            for para_masspoint in self.parametric_masspoints:
                if self.debug: print("Predicting mass ", para_masspoint)
                pred = self.predict(infile[key], int(para_masspoint))
                if self.debug:
                    print(len(pred[:,0]))
                    print(len(double_events))
                double_events['DNN_Signal_m'+para_masspoint] = pred[:,0]
                double_events['DNN_TT_m'+para_masspoint] = pred[:,1]
                double_events['DNN_DY_m'+para_masspoint] = pred[:,2]
                double_events['DNN_ST_m'+para_masspoint] = pred[:,3]
            syst = (key.split("Tree")[0]).split("Double_HME_MT2_")[1]
            outfile['Double_HME_MT2_DNN_'+syst+'Tree'] = double_events
